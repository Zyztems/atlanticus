﻿namespace Atlanticus.GUI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.tabControlAtlanticus = new System.Windows.Forms.TabControl();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.numericUpDownAutoReload = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDownAutoBattleNumber = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownTeleport = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTargetNew = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxHealPoints = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBoxLifePotions = new System.Windows.Forms.ComboBox();
            this.groupBoxWaterLife = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.checkBoxWaterLife = new System.Windows.Forms.CheckBox();
            this.numericUpDownDeadMercs = new System.Windows.Forms.NumericUpDown();
            this.comboBoxWaterLife = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxAutoBattle = new System.Windows.Forms.CheckBox();
            this.numericUpDownZoomLevel = new System.Windows.Forms.NumericUpDown();
            this.comboBoxMagicPoints = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPageColors = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonMobColorDefault = new System.Windows.Forms.Button();
            this.buttonColorMobFrom = new System.Windows.Forms.Button();
            this.buttonColorMobTo = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonMainColorDefault = new System.Windows.Forms.Button();
            this.buttonColorSkip = new System.Windows.Forms.Button();
            this.numericUpDownVariation = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPageLog = new System.Windows.Forms.TabPage();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonReload = new System.Windows.Forms.Button();
            this.toolTipHelper = new System.Windows.Forms.ToolTip(this.components);
            this.colorDialogTarget = new System.Windows.Forms.ColorDialog();
            this.checkBoxSit = new System.Windows.Forms.CheckBox();
            this.tabControlAtlanticus.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoReload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoBattleNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTeleport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTargetNew)).BeginInit();
            this.groupBoxWaterLife.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDeadMercs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZoomLevel)).BeginInit();
            this.tabPageColors.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVariation)).BeginInit();
            this.tabPageLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlAtlanticus
            // 
            this.tabControlAtlanticus.Controls.Add(this.tabPageSettings);
            this.tabControlAtlanticus.Controls.Add(this.tabPageColors);
            this.tabControlAtlanticus.Controls.Add(this.tabPageLog);
            this.tabControlAtlanticus.Location = new System.Drawing.Point(8, 12);
            this.tabControlAtlanticus.Name = "tabControlAtlanticus";
            this.tabControlAtlanticus.SelectedIndex = 0;
            this.tabControlAtlanticus.Size = new System.Drawing.Size(288, 377);
            this.tabControlAtlanticus.TabIndex = 0;
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Controls.Add(this.checkBoxSit);
            this.tabPageSettings.Controls.Add(this.numericUpDownAutoReload);
            this.tabPageSettings.Controls.Add(this.label5);
            this.tabPageSettings.Controls.Add(this.numericUpDownAutoBattleNumber);
            this.tabPageSettings.Controls.Add(this.label1);
            this.tabPageSettings.Controls.Add(this.numericUpDownTeleport);
            this.tabPageSettings.Controls.Add(this.numericUpDownTargetNew);
            this.tabPageSettings.Controls.Add(this.label3);
            this.tabPageSettings.Controls.Add(this.comboBoxHealPoints);
            this.tabPageSettings.Controls.Add(this.label21);
            this.tabPageSettings.Controls.Add(this.comboBoxLifePotions);
            this.tabPageSettings.Controls.Add(this.groupBoxWaterLife);
            this.tabPageSettings.Controls.Add(this.label2);
            this.tabPageSettings.Controls.Add(this.checkBoxAutoBattle);
            this.tabPageSettings.Controls.Add(this.numericUpDownZoomLevel);
            this.tabPageSettings.Controls.Add(this.comboBoxMagicPoints);
            this.tabPageSettings.Controls.Add(this.label16);
            this.tabPageSettings.Controls.Add(this.label18);
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(280, 351);
            this.tabPageSettings.TabIndex = 1;
            this.tabPageSettings.Text = "Settings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // numericUpDownAutoReload
            // 
            this.numericUpDownAutoReload.Location = new System.Drawing.Point(123, 319);
            this.numericUpDownAutoReload.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownAutoReload.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownAutoReload.Name = "numericUpDownAutoReload";
            this.numericUpDownAutoReload.Size = new System.Drawing.Size(105, 20);
            this.numericUpDownAutoReload.TabIndex = 60;
            this.numericUpDownAutoReload.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownAutoReload.ValueChanged += new System.EventHandler(this.NumericUpDownAutoReloadValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 321);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Relodad";
            // 
            // numericUpDownAutoBattleNumber
            // 
            this.numericUpDownAutoBattleNumber.Location = new System.Drawing.Point(138, 256);
            this.numericUpDownAutoBattleNumber.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownAutoBattleNumber.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownAutoBattleNumber.Name = "numericUpDownAutoBattleNumber";
            this.numericUpDownAutoBattleNumber.Size = new System.Drawing.Size(56, 20);
            this.numericUpDownAutoBattleNumber.TabIndex = 58;
            this.numericUpDownAutoBattleNumber.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownAutoBattleNumber.ValueChanged += new System.EventHandler(this.NumericUpDownAutoBattleNumberValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Slot - Healt Point";
            // 
            // numericUpDownTeleport
            // 
            this.numericUpDownTeleport.Location = new System.Drawing.Point(105, 220);
            this.numericUpDownTeleport.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownTeleport.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTeleport.Name = "numericUpDownTeleport";
            this.numericUpDownTeleport.Size = new System.Drawing.Size(56, 20);
            this.numericUpDownTeleport.TabIndex = 47;
            this.numericUpDownTeleport.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTeleport.ValueChanged += new System.EventHandler(this.NumericUpDownTeleportValueChanged);
            // 
            // numericUpDownTargetNew
            // 
            this.numericUpDownTargetNew.Location = new System.Drawing.Point(123, 293);
            this.numericUpDownTargetNew.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.numericUpDownTargetNew.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTargetNew.Name = "numericUpDownTargetNew";
            this.numericUpDownTargetNew.Size = new System.Drawing.Size(105, 20);
            this.numericUpDownTargetNew.TabIndex = 57;
            this.numericUpDownTargetNew.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTargetNew.ValueChanged += new System.EventHandler(this.NumericUpDownTargetNewValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Slot - Life Potion";
            // 
            // comboBoxHealPoints
            // 
            this.comboBoxHealPoints.FormattingEnabled = true;
            this.comboBoxHealPoints.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5"});
            this.comboBoxHealPoints.Location = new System.Drawing.Point(117, 13);
            this.comboBoxHealPoints.Name = "comboBoxHealPoints";
            this.comboBoxHealPoints.Size = new System.Drawing.Size(37, 21);
            this.comboBoxHealPoints.TabIndex = 3;
            this.comboBoxHealPoints.SelectedIndexChanged += new System.EventHandler(this.ComboBoxHealPointsSelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 295);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 13);
            this.label21.TabIndex = 56;
            this.label21.Text = "Target new mob";
            // 
            // comboBoxLifePotions
            // 
            this.comboBoxLifePotions.FormattingEnabled = true;
            this.comboBoxLifePotions.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5"});
            this.comboBoxLifePotions.Location = new System.Drawing.Point(117, 67);
            this.comboBoxLifePotions.Name = "comboBoxLifePotions";
            this.comboBoxLifePotions.Size = new System.Drawing.Size(37, 21);
            this.comboBoxLifePotions.TabIndex = 5;
            this.comboBoxLifePotions.SelectedIndexChanged += new System.EventHandler(this.ComboBoxLifePotionsSelectedIndexChanged);
            // 
            // groupBoxWaterLife
            // 
            this.groupBoxWaterLife.Controls.Add(this.label20);
            this.groupBoxWaterLife.Controls.Add(this.label19);
            this.groupBoxWaterLife.Controls.Add(this.checkBoxWaterLife);
            this.groupBoxWaterLife.Controls.Add(this.numericUpDownDeadMercs);
            this.groupBoxWaterLife.Controls.Add(this.comboBoxWaterLife);
            this.groupBoxWaterLife.Location = new System.Drawing.Point(6, 122);
            this.groupBoxWaterLife.Name = "groupBoxWaterLife";
            this.groupBoxWaterLife.Size = new System.Drawing.Size(257, 63);
            this.groupBoxWaterLife.TabIndex = 55;
            this.groupBoxWaterLife.TabStop = false;
            this.groupBoxWaterLife.Text = "Water of life";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(111, 32);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 56;
            this.label20.Text = "Dead mercs";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 32);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 13);
            this.label19.TabIndex = 55;
            this.label19.Text = "Slot";
            // 
            // checkBoxWaterLife
            // 
            this.checkBoxWaterLife.AutoSize = true;
            this.checkBoxWaterLife.Location = new System.Drawing.Point(79, 0);
            this.checkBoxWaterLife.Name = "checkBoxWaterLife";
            this.checkBoxWaterLife.Size = new System.Drawing.Size(15, 14);
            this.checkBoxWaterLife.TabIndex = 52;
            this.checkBoxWaterLife.UseVisualStyleBackColor = true;
            this.checkBoxWaterLife.CheckedChanged += new System.EventHandler(this.CheckBoxWaterLifeCheckedChanged);
            // 
            // numericUpDownDeadMercs
            // 
            this.numericUpDownDeadMercs.Location = new System.Drawing.Point(195, 30);
            this.numericUpDownDeadMercs.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownDeadMercs.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDeadMercs.Name = "numericUpDownDeadMercs";
            this.numericUpDownDeadMercs.Size = new System.Drawing.Size(37, 20);
            this.numericUpDownDeadMercs.TabIndex = 54;
            this.numericUpDownDeadMercs.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDeadMercs.ValueChanged += new System.EventHandler(this.NumericUpDownDeadMercsValueChanged);
            // 
            // comboBoxWaterLife
            // 
            this.comboBoxWaterLife.FormattingEnabled = true;
            this.comboBoxWaterLife.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5"});
            this.comboBoxWaterLife.Location = new System.Drawing.Point(47, 29);
            this.comboBoxWaterLife.Name = "comboBoxWaterLife";
            this.comboBoxWaterLife.Size = new System.Drawing.Size(37, 21);
            this.comboBoxWaterLife.TabIndex = 53;
            this.comboBoxWaterLife.SelectedIndexChanged += new System.EventHandler(this.ComboBoxWaterLifeSelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Slot - Magic Point";
            // 
            // checkBoxAutoBattle
            // 
            this.checkBoxAutoBattle.AutoSize = true;
            this.checkBoxAutoBattle.Location = new System.Drawing.Point(6, 257);
            this.checkBoxAutoBattle.Name = "checkBoxAutoBattle";
            this.checkBoxAutoBattle.Size = new System.Drawing.Size(116, 17);
            this.checkBoxAutoBattle.TabIndex = 48;
            this.checkBoxAutoBattle.Text = "AutoBattle Licence";
            this.checkBoxAutoBattle.UseVisualStyleBackColor = true;
            this.checkBoxAutoBattle.CheckedChanged += new System.EventHandler(this.CheckBoxAutoBattleCheckedChanged);
            // 
            // numericUpDownZoomLevel
            // 
            this.numericUpDownZoomLevel.Location = new System.Drawing.Point(105, 196);
            this.numericUpDownZoomLevel.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericUpDownZoomLevel.Minimum = new decimal(new int[] {
            13,
            0,
            0,
            0});
            this.numericUpDownZoomLevel.Name = "numericUpDownZoomLevel";
            this.numericUpDownZoomLevel.Size = new System.Drawing.Size(56, 20);
            this.numericUpDownZoomLevel.TabIndex = 50;
            this.numericUpDownZoomLevel.Value = new decimal(new int[] {
            13,
            0,
            0,
            0});
            this.numericUpDownZoomLevel.ValueChanged += new System.EventHandler(this.NumericUpDownZoomLevelValueChanged);
            // 
            // comboBoxMagicPoints
            // 
            this.comboBoxMagicPoints.FormattingEnabled = true;
            this.comboBoxMagicPoints.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5"});
            this.comboBoxMagicPoints.Location = new System.Drawing.Point(117, 40);
            this.comboBoxMagicPoints.Name = "comboBoxMagicPoints";
            this.comboBoxMagicPoints.Size = new System.Drawing.Size(37, 21);
            this.comboBoxMagicPoints.TabIndex = 4;
            this.comboBoxMagicPoints.SelectedIndexChanged += new System.EventHandler(this.ComboBoxMagicPointsSelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 222);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 13);
            this.label16.TabIndex = 45;
            this.label16.Text = "Teleport Favorite";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 198);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 49;
            this.label18.Text = "Zoom Level";
            // 
            // tabPageColors
            // 
            this.tabPageColors.Controls.Add(this.groupBox2);
            this.tabPageColors.Controls.Add(this.groupBox1);
            this.tabPageColors.Location = new System.Drawing.Point(4, 22);
            this.tabPageColors.Name = "tabPageColors";
            this.tabPageColors.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageColors.Size = new System.Drawing.Size(280, 351);
            this.tabPageColors.TabIndex = 4;
            this.tabPageColors.Text = "Colors";
            this.tabPageColors.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonMobColorDefault);
            this.groupBox2.Controls.Add(this.buttonColorMobFrom);
            this.groupBox2.Controls.Add(this.buttonColorMobTo);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.numericUpDown1);
            this.groupBox2.Location = new System.Drawing.Point(6, 90);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(268, 106);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mob";
            // 
            // buttonMobColorDefault
            // 
            this.buttonMobColorDefault.Location = new System.Drawing.Point(6, 74);
            this.buttonMobColorDefault.Name = "buttonMobColorDefault";
            this.buttonMobColorDefault.Size = new System.Drawing.Size(75, 23);
            this.buttonMobColorDefault.TabIndex = 21;
            this.buttonMobColorDefault.Text = "Default";
            this.buttonMobColorDefault.UseVisualStyleBackColor = true;
            this.buttonMobColorDefault.Click += new System.EventHandler(this.ButtonMobColorDefaultClick);
            // 
            // buttonColorMobFrom
            // 
            this.buttonColorMobFrom.BackColor = System.Drawing.Color.Transparent;
            this.buttonColorMobFrom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonColorMobFrom.Location = new System.Drawing.Point(6, 19);
            this.buttonColorMobFrom.Name = "buttonColorMobFrom";
            this.buttonColorMobFrom.Size = new System.Drawing.Size(256, 23);
            this.buttonColorMobFrom.TabIndex = 5;
            this.buttonColorMobFrom.Text = "Scan Color From";
            this.buttonColorMobFrom.UseVisualStyleBackColor = false;
            this.buttonColorMobFrom.Click += new System.EventHandler(this.ButtonColorMobFromClick);
            // 
            // buttonColorMobTo
            // 
            this.buttonColorMobTo.BackColor = System.Drawing.Color.Transparent;
            this.buttonColorMobTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonColorMobTo.Location = new System.Drawing.Point(6, 48);
            this.buttonColorMobTo.Name = "buttonColorMobTo";
            this.buttonColorMobTo.Size = new System.Drawing.Size(256, 23);
            this.buttonColorMobTo.TabIndex = 6;
            this.buttonColorMobTo.Text = "Scan Color To";
            this.buttonColorMobTo.UseVisualStyleBackColor = false;
            this.buttonColorMobTo.Click += new System.EventHandler(this.ButtonColorMobToClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(129, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Variation:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(186, 77);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(76, 20);
            this.numericUpDown1.TabIndex = 16;
            this.numericUpDown1.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonMainColorDefault);
            this.groupBox1.Controls.Add(this.buttonColorSkip);
            this.groupBox1.Controls.Add(this.numericUpDownVariation);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 78);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Main";
            // 
            // buttonMainColorDefault
            // 
            this.buttonMainColorDefault.Location = new System.Drawing.Point(6, 45);
            this.buttonMainColorDefault.Name = "buttonMainColorDefault";
            this.buttonMainColorDefault.Size = new System.Drawing.Size(75, 23);
            this.buttonMainColorDefault.TabIndex = 20;
            this.buttonMainColorDefault.Text = "Default";
            this.buttonMainColorDefault.UseVisualStyleBackColor = true;
            this.buttonMainColorDefault.Click += new System.EventHandler(this.ButtonMainColorDefaultClick);
            // 
            // buttonColorSkip
            // 
            this.buttonColorSkip.BackColor = System.Drawing.Color.Transparent;
            this.buttonColorSkip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonColorSkip.Location = new System.Drawing.Point(6, 19);
            this.buttonColorSkip.Name = "buttonColorSkip";
            this.buttonColorSkip.Size = new System.Drawing.Size(256, 23);
            this.buttonColorSkip.TabIndex = 4;
            this.buttonColorSkip.Text = "Skip Color";
            this.buttonColorSkip.UseVisualStyleBackColor = false;
            this.buttonColorSkip.Click += new System.EventHandler(this.ButtonColorSkipClick);
            // 
            // numericUpDownVariation
            // 
            this.numericUpDownVariation.Location = new System.Drawing.Point(186, 48);
            this.numericUpDownVariation.Name = "numericUpDownVariation";
            this.numericUpDownVariation.Size = new System.Drawing.Size(76, 20);
            this.numericUpDownVariation.TabIndex = 14;
            this.numericUpDownVariation.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(129, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Variation:";
            // 
            // tabPageLog
            // 
            this.tabPageLog.Controls.Add(this.textBoxLog);
            this.tabPageLog.Location = new System.Drawing.Point(4, 22);
            this.tabPageLog.Name = "tabPageLog";
            this.tabPageLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLog.Size = new System.Drawing.Size(280, 351);
            this.tabPageLog.TabIndex = 3;
            this.tabPageLog.Text = "Log";
            this.tabPageLog.UseVisualStyleBackColor = true;
            // 
            // textBoxLog
            // 
            this.textBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxLog.Location = new System.Drawing.Point(3, 3);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxLog.Size = new System.Drawing.Size(274, 345);
            this.textBoxLog.TabIndex = 0;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(221, 395);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStartClick);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(12, 395);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 2;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.ButtonStopClick);
            // 
            // buttonReload
            // 
            this.buttonReload.Location = new System.Drawing.Point(110, 395);
            this.buttonReload.Name = "buttonReload";
            this.buttonReload.Size = new System.Drawing.Size(75, 23);
            this.buttonReload.TabIndex = 3;
            this.buttonReload.Text = "Reload";
            this.buttonReload.UseVisualStyleBackColor = true;
            this.buttonReload.Click += new System.EventHandler(this.ButtonReloadClick);
            // 
            // colorDialogTarget
            // 
            this.colorDialogTarget.FullOpen = true;
            // 
            // checkBoxSit
            // 
            this.checkBoxSit.AutoSize = true;
            this.checkBoxSit.Location = new System.Drawing.Point(21, 96);
            this.checkBoxSit.Name = "checkBoxSit";
            this.checkBoxSit.Size = new System.Drawing.Size(62, 17);
            this.checkBoxSit.TabIndex = 61;
            this.checkBoxSit.Tag = "";
            this.checkBoxSit.Text = "Only Sit";
            this.checkBoxSit.UseVisualStyleBackColor = true;
            this.checkBoxSit.CheckedChanged += new System.EventHandler(this.CheckBoxSitCheckedChanged);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 425);
            this.Controls.Add(this.buttonReload);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.tabControlAtlanticus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "Atlanticus";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMainFormClosing);
            this.Load += new System.EventHandler(this.FrmMainLoad);
            this.tabControlAtlanticus.ResumeLayout(false);
            this.tabPageSettings.ResumeLayout(false);
            this.tabPageSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoReload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoBattleNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTeleport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTargetNew)).EndInit();
            this.groupBoxWaterLife.ResumeLayout(false);
            this.groupBoxWaterLife.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDeadMercs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZoomLevel)).EndInit();
            this.tabPageColors.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVariation)).EndInit();
            this.tabPageLog.ResumeLayout(false);
            this.tabPageLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlAtlanticus;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxLifePotions;
        private System.Windows.Forms.ComboBox comboBoxMagicPoints;
        private System.Windows.Forms.ComboBox comboBoxHealPoints;
        private System.Windows.Forms.CheckBox checkBoxAutoBattle;
        private System.Windows.Forms.NumericUpDown numericUpDownTeleport;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericUpDownZoomLevel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox checkBoxWaterLife;
        private System.Windows.Forms.NumericUpDown numericUpDownDeadMercs;
        private System.Windows.Forms.ComboBox comboBoxWaterLife;
        private System.Windows.Forms.GroupBox groupBoxWaterLife;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.NumericUpDown numericUpDownTargetNew;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoBattleNumber;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonReload;
        private System.Windows.Forms.ToolTip toolTipHelper;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoReload;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPageLog;
        public System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Button buttonColorMobFrom;
        private System.Windows.Forms.Button buttonColorSkip;
        private System.Windows.Forms.TabPage tabPageColors;
        private System.Windows.Forms.Button buttonColorMobTo;
        private System.Windows.Forms.NumericUpDown numericUpDownVariation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonMobColorDefault;
        private System.Windows.Forms.Button buttonMainColorDefault;
        private System.Windows.Forms.ColorDialog colorDialogTarget;
        private System.Windows.Forms.CheckBox checkBoxSit;
    }
}

