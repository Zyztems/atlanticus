﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Atlanticus.Game;
using Atlanticus.Properties;
using Atlanticus.Utilities.Input.Hook;

namespace Atlanticus.GUI
{
    public partial class FrmMain : Form
    {
        private Thread _workerThread;
        private static readonly List<Client> Clients = new List<Client>();
        private readonly KeyboardHook _globalKeyboardHook;
        private bool _status;

        private readonly Color _main = Color.FromArgb(65, 255, 0);
        private readonly Color _mobFrom = Color.FromArgb(0, 100, 0);
        private readonly Color _mobTo = Color.FromArgb(0, 255, 0);

        public FrmMain()
        {
            InitializeComponent();
            _globalKeyboardHook = new KeyboardHook();
            _globalKeyboardHook.KeyUp += KeyboardHookKeyUp;
        }

        private void KeyboardHookKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F6")
                if (!_status)
                    Start();
                else
                    Stop();
        }

        private void FrmMainLoad(object sender, EventArgs e)
        {
            LoadSettings();

            LoadBot();

            _globalKeyboardHook.Start();
            
            toolTipHelper.SetToolTip(comboBoxHealPoints, "Use food in your main");
            toolTipHelper.SetToolTip(comboBoxMagicPoints, "Use food in your main");
            toolTipHelper.SetToolTip(comboBoxLifePotions, "Use potion in your main");
        }

        private void LoadBot()
        {
            if (!Find()) 
                return;

            Text += String.Format(" [{0}]", Clients[0].GameClientMemory.GameProcessMemory.ReadProcess.MainWindowHandle);

            _workerThread = new Thread(Clients[0].Start);
        }

        #region Settings

        private void LoadSettings()
        {
            buttonColorSkip.BackColor =
                Settings.Default.ColorMainSkip =
                Settings.Default.ColorMainSkip.IsEmpty ? _main : Settings.Default.ColorMainSkip;
            buttonColorMobFrom.BackColor =
                Settings.Default.ColorMobAttackFrom =
                Settings.Default.ColorMobAttackFrom.IsEmpty ? _mobFrom : Settings.Default.ColorMobAttackFrom;
            buttonColorMobTo.BackColor =
                Settings.Default.ColorMobAttackTo =
                Settings.Default.ColorMobAttackTo.IsEmpty ? _mobTo : Settings.Default.ColorMobAttackTo;

            comboBoxHealPoints.SelectedIndex = Settings.Default.HealthPoint;
            comboBoxMagicPoints.SelectedIndex = Settings.Default.MagicPoint;
            comboBoxLifePotions.SelectedIndex = Settings.Default.LifePotion;

            numericUpDownAutoBattleNumber.Value = Settings.Default.AutoBattleNumber;
            numericUpDownTargetNew.Value = Settings.Default.TargetNewMob;
            numericUpDownZoomLevel.Value = Settings.Default.ZoomLevel;
            numericUpDownTeleport.Value = Settings.Default.Teleport;
            checkBoxAutoBattle.Checked = Settings.Default.AutoBattle;
            checkBoxWaterLife.Checked = Settings.Default.WaterLife;
            checkBoxSit.Checked = Settings.Default.OnlySit;
            comboBoxWaterLife.SelectedIndex = Settings.Default.WaterLifeSlot;
            numericUpDownDeadMercs.Value = Settings.Default.DeadMercs;
            comboBoxWaterLife.Enabled = Settings.Default.WaterLife;
            numericUpDownDeadMercs.Enabled = Settings.Default.WaterLife;
            numericUpDownAutoReload.Value = Settings.Default.AutoReload;
        }

        private void ComboBoxHealPointsSelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.HealthPoint = comboBoxHealPoints.SelectedIndex;
            Settings.Default.Save();
        }

        private void ComboBoxMagicPointsSelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.MagicPoint = comboBoxMagicPoints.SelectedIndex;
            Settings.Default.Save();
        }

        private void ComboBoxLifePotionsSelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.LifePotion = comboBoxLifePotions.SelectedIndex;
            Settings.Default.Save();
        }

        private void NumericUpDownTeleportValueChanged(object sender, EventArgs e)
        {
            Settings.Default.Teleport = numericUpDownTeleport.Value;
            Settings.Default.Save();
        }

        private void CheckBoxAutoBattleCheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.AutoBattle = checkBoxAutoBattle.Checked;
            Settings.Default.Save();
        }

        private void CheckBoxWaterLifeCheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.WaterLife = checkBoxWaterLife.Checked;
            Settings.Default.Save();

            comboBoxWaterLife.Enabled = checkBoxWaterLife.Checked;
            numericUpDownDeadMercs.Enabled = checkBoxWaterLife.Checked;
        }

        private void ComboBoxWaterLifeSelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.WaterLifeSlot = comboBoxWaterLife.SelectedIndex;
            Settings.Default.Save();
        }

        private void NumericUpDownDeadMercsValueChanged(object sender, EventArgs e)
        {
            Settings.Default.DeadMercs = numericUpDownDeadMercs.Value;
            Settings.Default.Save();
        }

        private void NumericUpDownZoomLevelValueChanged(object sender, EventArgs e)
        {
            Settings.Default.ZoomLevel = numericUpDownZoomLevel.Value;
            Settings.Default.Save();
        }

        private void NumericUpDownTargetNewValueChanged(object sender, EventArgs e)
        {
            Settings.Default.TargetNewMob = numericUpDownTargetNew.Value;
            Settings.Default.Save();
        }

        private void NumericUpDownAutoBattleNumberValueChanged(object sender, EventArgs e)
        {
            Settings.Default.AutoBattleNumber = numericUpDownAutoBattleNumber.Value;
            Settings.Default.Save();
        }

        private void NumericUpDownAutoReloadValueChanged(object sender, EventArgs e)
        {
            Settings.Default.AutoReload = numericUpDownAutoReload.Value;
            Settings.Default.Save();
        }

        #endregion

        private void ButtonStartClick(object sender, EventArgs e)
        {
            Settings.Default.Save();
            Start(); 
        }

        private void Start()
        {
            try
            {
                if (Clients.Count > 0)
                {
                    _workerThread = new Thread(Clients[0].Start);
                    _workerThread.Start();
                    _status = true;
                    buttonReload.Enabled = false;
                    buttonStart.Enabled = false;
                }
            }
            catch (Exception exception)
            {
                textBoxLog.Text += string.Format("{0}\n", exception.Message);
                Stop(); 
            }
        }

        private void ButtonStopClick(object sender, EventArgs e)
        {
            Stop(); 
        }

        private void Stop()
        {
            try
            {
                Clients[0].Stop();
                if (_workerThread != null && _workerThread.IsAlive)
                {
                    _workerThread.Abort();
                    _workerThread.Join();
                }
                _status = false;
                buttonReload.Enabled = true;
                buttonStart.Enabled = true;
            }
            catch (Exception exception)
            {
                textBoxLog.Text += string.Format("{0}\n", exception.Message);
            }
            
        }

        private void FrmMainFormClosing(object sender, FormClosingEventArgs e)
        {
            Stop();
            _globalKeyboardHook.Stop();
        }

        private void ButtonReloadClick(object sender, EventArgs e)
        {
            LoadBot();
        }

        private bool Find()
        {
            Clients.Clear();
            var processesByName = Process.GetProcessesByName("Atlantica");
            foreach (var process in processesByName)
            {
                Clients.Add(new Client(new Utilities.Memory.ProcessMemory(process)));
            }
            return Clients.Count != 0;
        }

        private void ButtonColorSkipClick(object sender, EventArgs e)
        {
            colorDialogTarget.Color = Settings.Default.ColorMainSkip;
            if (colorDialogTarget.ShowDialog() == DialogResult.OK)
            {
                buttonColorSkip.BackColor = Settings.Default.ColorMainSkip = colorDialogTarget.Color;
                Settings.Default.Save();
            }
        }
        
        private void ButtonMainColorDefaultClick(object sender, EventArgs e)
        {
            Settings.Default.ColorMainSkip = Color.Empty;
            Settings.Default.Save();
        }

        private void ButtonMobColorDefaultClick(object sender, EventArgs e)
        {
            Settings.Default.ColorMobAttackFrom = Color.Empty;
            Settings.Default.ColorMobAttackTo = Color.Empty;
            Settings.Default.Save();
        }

        private void ButtonColorMobFromClick(object sender, EventArgs e)
        {
            colorDialogTarget.Color = Settings.Default.ColorMobAttackFrom;
            if (colorDialogTarget.ShowDialog() == DialogResult.OK)
            {
                buttonColorMobFrom.BackColor = Settings.Default.ColorMobAttackFrom = colorDialogTarget.Color;
                Settings.Default.Save();
            }
        }

        private void ButtonColorMobToClick(object sender, EventArgs e)
        {
            colorDialogTarget.Color = Settings.Default.ColorMobAttackTo;
            if (colorDialogTarget.ShowDialog() == DialogResult.OK)
            {
                buttonColorMobTo.BackColor = Settings.Default.ColorMobAttackTo = colorDialogTarget.Color;
                Settings.Default.Save();
            }
        }

        private void CheckBoxSitCheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.OnlySit = checkBoxSit.Checked;
            Settings.Default.Save();
        }
    }
}
