﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Atlanticus.Utilities.Input;
using Atlanticus.Utilities.Screen;
using Atlanticus.Utilities.Search;
using Atlanticus.Utilities.WindowsAPI;

namespace Atlanticus.GUI
{
    public partial class FrmTest : Form
    {
        private Direct3DCapture _capture;
        private Thread _workerThread;
        private bool _status;
        private List<bool> _match = new List<bool>();

        public FrmTest()
        {
            InitializeComponent();
        }

        private void FrmTest_Load(object sender, EventArgs e)
        {
            _capture = new Direct3DCapture();
            _status = true;
        }

        private void shot()
        {
            do
            {
                var processesByName = Process.GetProcessesByName("Text3D");
                var screenCapture = _capture.CaptureWindow(processesByName[0].MainWindowHandle);
                _match.Add(PixelSearching.HasPixel(screenCapture, Color.FromArgb(65, 255, 0), 10));
                pictureBox1.Image = screenCapture;
                Thread.Sleep(1);
            } while (_status);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _match.Clear();
            _workerThread = new Thread(shot);
            _status = true;
            _workerThread.Start();
            //var processesByName = Process.GetProcessesByName("Text3D");
            //using (var screenCapture = _capture.CaptureWindow(processesByName[0].MainWindowHandle))
            //{
                //var screenCapture = _capture.CaptureWindow(processesByName[0].MainWindowHandle);
                //pictureBox1.Image = screenCapture;
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _status = true;
            _workerThread.Abort();
            foreach (var b in _match)
            {
                textBox1.Text += string.Format("{0}\n", b);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var processesByName = Process.GetProcessesByName("Atlantica");
            foreach (var process in processesByName)
            {
                User32.SetForegroundWindow(process.MainWindowHandle);
                Thread.Sleep(500);
                VirtualMouse.Wheel(-120 * 10);
            }
        }
    }
}
