﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using Atlanticus.Properties;
using Atlanticus.Utilities.AForge.Core;
using Atlanticus.Utilities.AForge.Imaging;
using Atlanticus.Utilities.AForge.Imaging.Filters;
using Atlanticus.Utilities.Input;
using Atlanticus.Utilities.Screen;
using Atlanticus.Utilities.Search;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;

namespace Atlanticus.Game
{
    public class BotScript
    {
        private ClientWindow _clientWindow;
        private ClientMemory _clientMemory;
        private readonly List<int> _hp = new List<int>();
        private readonly List<int> _mp = new List<int>();
        private Direct3DCapture _capture;
        private bool _status;

        public void Stop(bool status)
        {
            _status = status;
        }

        public void Start(ClientMemory gameClientMemory)
        {
            _clientMemory = gameClientMemory;
            _clientWindow = new ClientWindow(gameClientMemory.GameProcessMemory.ReadProcess.MainWindowHandle);
            _capture = new Direct3DCapture();
            //_status = _clientWindow.SupportedSize() && Load();
            _status = Load();

            while (_status)
            {
                Bot();
                Thread.Sleep(10);
            }
        }

        private bool Load()
        {
            try
            {
                _hp.Clear();
                _mp.Clear();
                for (var i = 0; i < 9; i++)
                {
                    _hp.Add((int)_clientMemory.Life(i));
                    _mp.Add((int)_clientMemory.Mana(i));
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void Bot()
        {
            if (_clientMemory.CombatInProcess()) Combat();
            
            BringToFront();
            BeforeAfterBattle();

            ShowGui(false);
            
            do
            {
                BirdView();
                var screenCapture = _capture.CaptureWindow(_clientMemory.GameProcessMemory.ReadProcess.MainWindowHandle);

                new ColorFiltering { Red = new IntRange(Settings.Default.ColorMobAttackFrom.R, 
                                                            Settings.Default.ColorMobAttackTo.R),
                                     Green = new IntRange(Settings.Default.ColorMobAttackFrom.G,
                                                            Settings.Default.ColorMobAttackTo.G),
                                     Blue = new IntRange(Settings.Default.ColorMobAttackFrom.B,
                                                            Settings.Default.ColorMobAttackTo.B)
                                     }.ApplyInPlace(screenCapture);

                if (!SearchMonsters(screenCapture))
                {
                    screenCapture.Dispose();
                    continue;
                }
                screenCapture.Dispose();
                
                for (var i = 0; i < Settings.Default.TargetNewMob; i++)
                {
                    Thread.Sleep(1);
                    if (_clientMemory.CombatInProcess()) break;
                }
            } while (!_clientMemory.CombatInProcess());

            ShowGui(true);

            Combat();
        }

        private void BringToFront()
        {
            User32.SetForegroundWindow(_clientMemory.GameProcessMemory.ReadProcess.MainWindowHandle);
            Thread.Sleep(500);
        }

        private void Combat()
        {
            VirtualMouse.Move(_clientWindow.ClientRectangle.X, _clientWindow.ClientRectangle.Y);
            do
            {
                ShowGui(true);
                AutoBattle();
                Thread.Sleep(500);
            } while (_clientMemory.CombatInProcess());
        }

        private void BirdView()
        {
            VirtualMouse.LeftDrag(new System.Drawing.Point(_clientWindow.ClientCenter.X, _clientWindow.ClientCenter.Y - 50),
                                  _clientWindow.ClientCenter);
            _clientMemory.ZoomBound();
            VirtualMouse.Wheel((int)(-120 * Settings.Default.ZoomLevel));
            Thread.Sleep(500);
        }

        private bool SearchMonsters(Bitmap bitmap)
        {
            var bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, bitmap.PixelFormat);

            var blobCounter = new BlobCounter { FilterBlobs = true, MinHeight = 5, MinWidth = 5 };
            blobCounter.ProcessImage(bitmapData);
        
            var blobs = blobCounter.GetObjectsInformation();

            bitmap.UnlockBits(bitmapData);

            BringToFront();

            var target = false;
            foreach (var blob in blobs)
            {
                var x = _clientWindow.ClientRectangle.Left + (int) blob.CenterOfGravity.X;
                var y = _clientWindow.ClientRectangle.Top + (int) blob.CenterOfGravity.Y;

                if (!_clientWindow.ClientRectangle.Contains(x, y)) continue;

                VirtualMouse.Move(x, y);
                Thread.Sleep(50);

                if (ClientMemory.SystemCursor() != _clientMemory.CursorSword()) continue;

                VirtualMouse.LeftClick(x, y);
                bitmap.Dispose();
                target = true;
                break;
            }

            return target;
        }

        private void ShowGui(bool hideShow)
        {

            var guiState =
                PixelSearching.GetPixelColor(_clientWindow.Coin) == Color.FromArgb(251, 215, 44);

            if (!hideShow && guiState)
            {
                VirtualKeyboard.KeyPress(VirtualKey.VK_W, false, false, true);
                Thread.Sleep(250);
            }
            if (hideShow && !guiState)
            {
                VirtualKeyboard.KeyPress(VirtualKey.VK_W, false, false, true);
                Thread.Sleep(250);
            }

        }

        private void AutoBattle()
        {
            if (Settings.Default.AutoBattle)
            {
                if (_clientMemory.CombatInProcess() && !_clientMemory.AutoBattleActive())
                {
                    if (_clientMemory.AutoBattleNumber() >= Settings.Default.AutoBattleNumber)
                    {
                        do
                        {
                            VirtualMouse.LeftClick(_clientWindow.AutoBattle);
                            Thread.Sleep(1000);
                        } while (!_clientMemory.AutoBattleActive() && _clientMemory.CombatInProcess());
                    }
                    else
                    {
                        SkipMainTurn();
                    }
                }
            }
            else
            {
                if (_clientMemory.CombatInProcess() && !_clientMemory.AutoBattleActive())
                {
                    SkipMainTurn();
                }
            }
        }

        private void SkipMainTurn()
        {
            BringToFront();

            using (var imageScreen = _capture.CaptureWindow(_clientMemory.GameProcessMemory.ReadProcess.MainWindowHandle))
            {
                if (!PixelSearching.HasPixel(imageScreen, Color.FromArgb(Settings.Default.ColorMainSkip.R, Settings.Default.ColorMainSkip.G, Settings.Default.ColorMainSkip.B), (int)Settings.Default.ColorMainVariant))
                    return;
            }

            VirtualKeyboard.KeyPress(VirtualKey.VK_F);
            Thread.Sleep(100);
        }

        private void CloseWindows()
        {
            BringToFront();

            do
            {
                VirtualKeyboard.KeyPress(VirtualKey.VK_ESCAPE);
                Thread.Sleep(150);
            } while (PixelSearching.GetPixelColor(_clientWindow.CloseLocation) != Color.FromArgb(255, 255, 255) &&
                     PixelSearching.GetPixelColor(_clientWindow.CloseLocation2) != Color.FromArgb(255, 255, 255) &&
                     PixelSearching.GetPixelColor(_clientWindow.CloseLocation3) != Color.FromArgb(0, 0, 0));

            VirtualKeyboard.KeyPress(VirtualKey.VK_ESCAPE);
            Thread.Sleep(150);
        }

        private void BeforeAfterBattle()
        {
            if (_clientMemory.CombatInProcess())
                return;

            ShowGui(true);

            CloseWindows();
            Resurect();
            Heal();
            GrindPlace();
        }

        private void Resurect()
        {
            var deads = 0;
            for (var i = 0; i < 9; i++)
            {
                if (_clientMemory.Life(i) == 0)
                    deads++;
            }
            if (deads == 0) return;

            BringToFront();
            ShowGui(true);

            if (Settings.Default.WaterLife && deads >= Settings.Default.DeadMercs)
            {
                BringToFront();
                VirtualKeyboard.KeyPress(VirtualKey.VK_1);
                Thread.Sleep(150);
                VirtualKeyboard.KeyPress(KeyToSlot(Settings.Default.WaterLifeSlot));
                Thread.Sleep(150);
            }
            else
            {
                VirtualKeyboard.KeyPress(VirtualKey.VK_1);
                Thread.Sleep(150);
                VirtualKeyboard.KeyPress(KeyToSlot(Settings.Default.LifePotion));
                Thread.Sleep(150);
                for (var i = 0; i < deads; i++)
                {
                    BringToFront();

                    VirtualMouse.LeftClick(_clientWindow.Resurect);
                    Thread.Sleep(500);
                }
            }
            Resurect();
        }

        private void Heal()
        {
            if (_clientMemory.CombatInProcess())
                return;

            BringToFront();

            VirtualKeyboard.KeyPress(VirtualKey.VK_1);
            Thread.Sleep(150);
            VirtualKeyboard.KeyPress(VirtualKey.VK_X);
            Thread.Sleep(150);

            var stopWatch = new Stopwatch();
            bool fullHealed;
            do
            {
                stopWatch.Start();
                BringToFront();
                fullHealed = true;
                for (var i = 0; i < 9; i++)
                {
                    if (_clientMemory.Life(i) < _hp[i])
                    {
                        if (!Settings.Default.OnlySit) 
                            VirtualKeyboard.KeyPress(KeyToSlot(Settings.Default.HealthPoint));
                        fullHealed = false;
                        Thread.Sleep(250);
                    }
                    if (_clientMemory.Mana(i) < _mp[i])
                    {
                        if (!Settings.Default.OnlySit) 
                            VirtualKeyboard.KeyPress(KeyToSlot(Settings.Default.MagicPoint));
                        fullHealed = false;
                        Thread.Sleep(250);
                    }
                }

                if (stopWatch.Elapsed.Minutes >= Settings.Default.AutoReload)
                {
                    Load();
                    stopWatch.Reset();
                    stopWatch.Start();
                }
            } while (!fullHealed);
        }

        private void GrindPlace()
        {
            if (_clientMemory.CombatInProcess())
                return;

            while (!_clientMemory.MapName().ToLower().Contains("skirmish"))
            {
                Teleport();
            }
        }

        private void Teleport()
        {
            ShowGui(true);
            Thread.Sleep(150);
            VirtualKeyboard.KeyPress(VirtualKey.VK_B, false, false, true);
            Thread.Sleep(2000);

            VirtualMouse.LeftClick(_clientWindow.Teleport);
            Thread.Sleep(150);
        }
        
        private static VirtualKey KeyToSlot(int index)
        {
            switch (index)
            {
                case 0:
                    return VirtualKey.VK_F1;
                case 1:
                    return VirtualKey.VK_F2;
                case 2:
                    return VirtualKey.VK_F3;
                case 3:
                    return VirtualKey.VK_F4;
                case 4:
                    return VirtualKey.VK_F5;
                default:
                    return VirtualKey.VK_F12;
            }
        }
    }
}
