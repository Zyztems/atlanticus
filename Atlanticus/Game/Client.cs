﻿using Atlanticus.Utilities.Memory;

namespace Atlanticus.Game
{
    public class Client
    {
        public readonly ClientMemory GameClientMemory;
        private BotScript _botScript;

        public Client(ProcessMemory processGame)
        {
            GameClientMemory = new ClientMemory(processGame);
        }

        public void Start()
        {
            _botScript = new BotScript();
            _botScript.Start(GameClientMemory);
        }

        public void Stop()
        {
            _botScript.Stop(false);
        }
    }
}
