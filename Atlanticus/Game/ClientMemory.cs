﻿using Atlanticus.Properties;
using Atlanticus.Utilities.IniFileParser;
using Atlanticus.Utilities.Memory;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Structure;

namespace Atlanticus.Game
{
    public class ClientMemory
    {
        public readonly ProcessMemory GameProcessMemory;
        private readonly IniData _iniData;

        public ClientMemory(ProcessMemory gameProcessMemory)
        {
            GameProcessMemory = gameProcessMemory;
            _iniData = new FileIniDataParser().LoadFile("Memory.ini");
        }

        private static int HexStringToInt(string hexValue)
        {
            return int.Parse(hexValue.Substring(2), System.Globalization.NumberStyles.HexNumber);
        }

        public uint Life(int character)
        {
            return GameProcessMemory.ReadInt(HexStringToInt(_iniData["Life"]["Static"]),
                                                            new []
                                                            {
                                                                HexStringToInt(_iniData["Life"]["Offset1"]), 
                                                                HexStringToInt(_iniData["Life"]["Offset2"]), 
                                                                HexStringToInt(_iniData["Life"]["Offset3"]), 
                                                                HexStringToInt(_iniData["Life"]["Offset4"]) + (HexStringToInt(_iniData["CharacterOffset"]["Static"])) * character
                                                            });

        }

        public uint Mana(int character)
        {
            return GameProcessMemory.ReadInt(HexStringToInt(_iniData["Mana"]["Static"]),
                                                            new[]
                                                            {
                                                                HexStringToInt(_iniData["Mana"]["Offset1"]), 
                                                                HexStringToInt(_iniData["Mana"]["Offset2"]), 
                                                                HexStringToInt(_iniData["Mana"]["Offset3"]), 
                                                                HexStringToInt(_iniData["Mana"]["Offset4"]) + (HexStringToInt(_iniData["CharacterOffset"]["Static"])) * character
                                                            });

        }

        public string MapName()
        {
            return GameProcessMemory.ReadText(HexStringToInt(_iniData["MapName"]["Static"]),
                                                            new[]
                                                            {
                                                                HexStringToInt(_iniData["MapName"]["Offset1"]), 
                                                                HexStringToInt(_iniData["MapName"]["Offset2"]), 
                                                                HexStringToInt(_iniData["MapName"]["Offset3"]), 
                                                                HexStringToInt(_iniData["MapName"]["Offset4"]),
                                                                HexStringToInt(_iniData["MapName"]["Offset5"])
                                                            }, 100).Replace("\0", "");

        }

        public uint CursorSword()
        {
            return GameProcessMemory.ReadInt(HexStringToInt(_iniData["CursorSword"]["Static"]),
                                                            new[]
                                                            {
                                                                HexStringToInt(_iniData["CursorSword"]["Offset1"]), 
                                                                HexStringToInt(_iniData["CursorSword"]["Offset2"])
                                                            });
        }

        public void ZoomBound()
        {
            GameProcessMemory.Write(HexStringToInt(_iniData["ZoomBound"]["Static"]),
                                                            new[]
                                                            {
                                                                HexStringToInt(_iniData["ZoomBound"]["Offset1"]), 
                                                                HexStringToInt(_iniData["ZoomBound"]["Offset2"])
                                                            }, (float)Settings.Default.ZoomLevel);
        }

        public bool CombatInProcess()
        {
            return GameProcessMemory.ReadInt(HexStringToInt(_iniData["CombatInProcess"]["Static"])) > 0;
        }

        public bool AutoBattleActive()
        {
            return GameProcessMemory.ReadInt(HexStringToInt(_iniData["AutoBattleActive"]["Static"]),
                                                            new[]
                                                            {
                                                                HexStringToInt(_iniData["AutoBattleActive"]["Offset1"])
                                                            }) > 0;
        }

        public uint AutoBattleNumber()
        {
            return GameProcessMemory.ReadInt(HexStringToInt(_iniData["AutoBattleNumber"]["Static"]),
                                                            new[]
                                                            {
                                                                HexStringToInt(_iniData["AutoBattleNumber"]["Offset1"])
                                                            });
        }

        public static int SystemCursor()
        {
            CursorInformation pci;
            pci.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CursorInformation));
            User32.GetCursorInfo(out pci);

            return pci.hCursor.ToInt32();
        }
    }
}
