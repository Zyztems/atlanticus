﻿using System;
using System.Drawing;
using Atlanticus.Properties;
using Atlanticus.Utilities.Screen;
using Atlanticus.Utilities.WindowControl;

namespace Atlanticus.Game
{
    public sealed class ClientWindow
    {
        private readonly IntPtr _handle;

        public ClientWindow(IntPtr handle)
        {
            _handle = handle;
        }

        public Point ClientCenter 
        { 
            get
            {
                return new Point(ClientRectangle.Left + (ClientRectangle.Width / 2),
                                 ClientRectangle.Top + (ClientRectangle.Height / 2));
            } 
        }

        public Point AutoBattle
        {
            get
            {
                return new Point(ClientRectangle.Right - 25, ClientRectangle.Bottom - 160);
            }
        }

        public Point Resurect 
        { 
            get
            {
                return new Point(ClientRectangle.Left + 565, ClientRectangle.Top + 270); 
            }
        }

        public Point Teleport
        {
            get
            {
                return new Point(ClientRectangle.Left + 375, ClientRectangle.Top + 195 + ((int)(Settings.Default.Teleport - 1) * 30));
            }
        }

        public Point Coin
        {
            get
            {
                return new Point(ClientRectangle.Left + 172, ClientRectangle.Bottom - 36);
            }
        }

        public Point CloseLocation
        {
            get
            {
                switch (CurrentSize)
                {
                    case ScreenSize.Size1024x768:
                        {
                            return new Point(ClientRectangle.Left + 385, ClientRectangle.Top + 170);
                        }
                    case ScreenSize.Size1280x1024:
                        {
                            return new Point(ClientRectangle.Left + 515, ClientRectangle.Top + 300);
                        }
                    case ScreenSize.Size1600x1200:
                        {
                            return new Point(ClientRectangle.Left + 675, ClientRectangle.Top + 380);
                        }
                    case ScreenSize.Size1768x992:
                        {
                            return new Point(ClientRectangle.Left + 775, ClientRectangle.Top + 295);
                        }
                    case ScreenSize.Size1920x1080:
                        {
                            return new Point(ClientRectangle.Left + 835, ClientRectangle.Top + 330);
                        }
                    default:
                        {
                            throw new Exception("Selected resolution not yet supported");
                        }
                }
            }
        }

        public Point CloseLocation2
        {
            get
            {
                switch (CurrentSize)
                {
                    case ScreenSize.Size1024x768:
                        {
                            return new Point(ClientRectangle.Left + 385, ClientRectangle.Top + 175);
                        }
                    case ScreenSize.Size1280x1024:
                        {
                            return new Point(ClientRectangle.Left + 515, ClientRectangle.Top + 305);
                        }
                    case ScreenSize.Size1600x1200:
                        {
                            return new Point(ClientRectangle.Left + 675, ClientRectangle.Top + 385);
                        }
                    case ScreenSize.Size1768x992:
                        {
                            return new Point(ClientRectangle.Left + 775, ClientRectangle.Top + 300);
                        }
                    case ScreenSize.Size1920x1080:
                        {
                            return new Point(ClientRectangle.Left + ClientRectangle.Left + 835, 335);
                        }
                    default:
                        {
                            throw new Exception("Selected resolution not yet supported");
                        }
                }
            }
        }

        public Point CloseLocation3
        {
            get
            {
                switch (CurrentSize)
                {
                    case ScreenSize.Size1024x768:
                        {
                            return new Point(ClientRectangle.Left + 600, ClientRectangle.Top + 525);
                        }
                    case ScreenSize.Size1280x1024:
                        {
                            return new Point(ClientRectangle.Left + 735, ClientRectangle.Top + 660);
                        }
                    case ScreenSize.Size1600x1200:
                        {
                            return new Point(ClientRectangle.Left + 895, ClientRectangle.Top + 740);
                        }
                    case ScreenSize.Size1768x992:
                        {
                            return new Point(ClientRectangle.Left + 980, ClientRectangle.Top + 645);
                        }
                    case ScreenSize.Size1920x1080:
                        {
                            return new Point(ClientRectangle.Left + 1055, ClientRectangle.Top + 690);
                        }
                    default:
                        {
                            throw new Exception("Selected resolution not yet supported");
                        }
                }
            }
        }

        public Rectangle ClientRectangle
        {
            get
            {
                return WindowPosition.GetAbsoluteClientRectangle(_handle);
            }
        }

        private ScreenSize CurrentSize
        {
            get
            {
                try
                {

                    return
                        (ScreenSize)
                        Enum.Parse(typeof (ScreenSize),
                                   string.Concat(new object[]
                                                     {"Size", ClientRectangle.Width, "x", ClientRectangle.Height}));
                }
                catch
                {
                    return ScreenSize.SizeUnknown;
                }
            }
        }

        //public bool SupportedSize()
        //{
        //    switch (CurrentSize)
        //    {
        //        case ScreenSize.Size1024x768:
        //        case ScreenSize.Size1280x1024:
        //        case ScreenSize.Size1600x1200:
        //        case ScreenSize.Size1768x992:
        //        case ScreenSize.Size1920x1080:
        //            {
        //                return true;
        //            }
        //        default:
        //            {
        //                throw new Exception("Selected resolution not yet supported");
        //            }
        //    }
        //}
    }
}
