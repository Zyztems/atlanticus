﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Atlanticus.Utilities.WindowsAPI;

namespace Atlanticus.Utilities.Input.Hook
{
    /// <summary>
    /// Abstract base class for Mouse and KeyboardEvent hooks
    /// </summary>
    public abstract class GlobalHook
    {
        #region Private Variables

        protected int HookType;
        protected IntPtr HandleToHook;
        private User32.HookProc _hookCallback;

        #endregion

        #region Properties

        public bool IsStarted { get; private set; }

        #endregion

        #region Constructor

        protected GlobalHook()
        {
            Application.ApplicationExit += ApplicationApplicationExit;
        }

        #endregion

        #region Methods

        public void Start()
        {
            if (IsStarted || HookType == 0) return;

            // Make sure we keep a reference to this delegate!
            // If not, GC randomly collects it, and a NullReference exception is thrown
            _hookCallback = HookCallbackProcedure;

            HandleToHook = User32.SetWindowsHookEx(
                HookType,
                _hookCallback,
                Process.GetCurrentProcess().MainModule.BaseAddress, //Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0])
                0);

            if (HandleToHook != IntPtr.Zero) // Were we able to sucessfully start hook?
                IsStarted = true;

        }

        public void Stop()
        {
            if (!IsStarted) return;

            User32.UnhookWindowsHookEx(HandleToHook);

            IsStarted = false;
        }

        protected virtual int HookCallbackProcedure(int nCode, Int32 wParam, IntPtr lParam)
        {
            return 0; // This method must be overriden by each extending hook
        }

        private void ApplicationApplicationExit(object sender, EventArgs e)
        {
            if (IsStarted)
                Stop();
        }

        #endregion

    }

}
