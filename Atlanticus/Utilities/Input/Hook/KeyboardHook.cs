﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;
using Atlanticus.Utilities.WindowsAPI.Structure;

namespace Atlanticus.Utilities.Input.Hook
{

    /// <summary>
    /// Captures global keyboard events
    /// </summary>
    public class KeyboardHook : GlobalHook
    {

        #region Events

        public event KeyEventHandler KeyDown;
        public event KeyEventHandler KeyUp;
        public event KeyPressEventHandler KeyPress;

        #endregion

        #region Constructor

        public KeyboardHook()
        {

            HookType = (int)HookTypes.WH_KEYBOARD_LL;

        }

        #endregion

        #region Methods

        protected override int HookCallbackProcedure(int nCode, int wParam, IntPtr lParam)
        {

            var handled = false;

            if (nCode > -1 && (KeyDown != null || KeyUp != null || KeyPress != null))
            {

                var keyboardHookStruct =
                    (KeyboardHookStruct)Marshal.PtrToStructure(lParam, typeof(KeyboardHookStruct));

                // Is Control being held down?
                var control = ((User32.GetKeyState((int)VirtualKey.VK_LCONTROL) & 0x80) != 0) ||
                               ((User32.GetKeyState((int)VirtualKey.VK_RCONTROL) & 0x80) != 0);

                // Is Shift being held down?
                var shift = ((User32.GetKeyState((int)VirtualKey.VK_LSHIFT) & 0x80) != 0) ||
                             ((User32.GetKeyState((int)VirtualKey.VK_RSHIFT) & 0x80) != 0);

                // Is Alt being held down?
                var alt = ((User32.GetKeyState((int)VirtualKey.VK_LMENU) & 0x80) != 0) ||
                           ((User32.GetKeyState((int)VirtualKey.VK_RMENU) & 0x80) != 0);

                // Is CapsLock on?
                var capslock = (User32.GetKeyState((int)VirtualKey.VK_CAPITAL) != 0);

                // Create event using keycode and control/shift/alt values found above
                var e = new KeyEventArgs(
                    (Keys)(
                        keyboardHookStruct.vkCode |
                        (control ? (int)Keys.Control : 0) |
                        (shift ? (int)Keys.Shift : 0) |
                        (alt ? (int)Keys.Alt : 0)
                        ));

                // Handle KeyDown and KeyUp events
                switch (wParam)
                {

                    case (int)WindowMessages.WM_KEYDOWN:
                    case (int)WindowMessages.WM_SYSKEYDOWN:
                        if (KeyDown != null)
                        {
                            KeyDown(this, e);
                            handled = e.Handled;
                        }
                        break;
                    case (int)WindowMessages.WM_KEYUP:
                    case (int)WindowMessages.WM_SYSKEYUP:
                        if (KeyUp != null)
                        {
                            KeyUp(this, e);
                            handled = e.Handled;
                        }
                        break;

                }

                // Handle KeyPress event
                if (wParam == (int)WindowMessages.WM_KEYDOWN &&
                   !handled &&
                   !e.SuppressKeyPress &&
                    KeyPress != null)
                {

                    var keyState = new byte[256];
                    var inBuffer = new byte[2];
                    User32.GetKeyboardState(keyState);

                    if (User32.ToAscii(keyboardHookStruct.vkCode,
                              keyboardHookStruct.scanCode,
                              keyState,
                              inBuffer,
                              keyboardHookStruct.flags) == 1)
                    {

                        var key = (char)inBuffer[0];
                        if ((capslock ^ shift) && Char.IsLetter(key))
                            key = Char.ToUpper(key);
                        var e2 = new KeyPressEventArgs(key);
                        KeyPress(this, e2);
                        handled = e.Handled;

                    }

                }

            }

            return handled ? 1 : User32.CallNextHookEx(HandleToHook, nCode, wParam, lParam);

        }

        #endregion

    }

}
