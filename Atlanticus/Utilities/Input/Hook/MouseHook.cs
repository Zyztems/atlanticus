﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;
using Atlanticus.Utilities.WindowsAPI.Structure;

namespace Atlanticus.Utilities.Input.Hook
{

    /// <summary>
    /// Captures global mouse events
    /// </summary>
    public class MouseHook : GlobalHook
    {

        #region MouseEventType Enum

        private enum MouseEventType
        {
            None,
            MouseDown,
            MouseUp,
            DoubleClick,
            MouseWheel,
            MouseMove
        }

        #endregion

        #region Events

        public event MouseEventHandler MouseDown;
        public event MouseEventHandler MouseUp;
        public event MouseEventHandler MouseMove;
        public event MouseEventHandler MouseWheel;

        public event EventHandler Click;
        public event EventHandler DoubleClick;

        #endregion

        #region Constructor

        public MouseHook()
        {

            HookType = (int)HookTypes.WH_MOUSE_LL;

        }

        #endregion

        #region Methods

        protected override int HookCallbackProcedure(int nCode, int wParam, IntPtr lParam)
        {
            
            if (nCode > -1 && (MouseDown != null || MouseUp != null || MouseMove != null))
            {

                var mouseHookStruct =
                    (MouseLlHookStruct)Marshal.PtrToStructure(lParam, typeof(MouseLlHookStruct));

                var button = GetButton(wParam);
                var eventType = GetEventType(wParam);

                var e = new MouseEventArgs(
                    button,
                    (eventType == MouseEventType.DoubleClick ? 2 : 1),
                    mouseHookStruct.pt.x,
                    mouseHookStruct.pt.y,
                    (eventType == MouseEventType.MouseWheel ? (short)((mouseHookStruct.mouseData >> 16) & 0xffff) : 0));

                // Prevent multiple Right Click events (this probably happens for popup menus)
                if (button == MouseButtons.Right && mouseHookStruct.flags != 0)
                {
                    eventType = MouseEventType.None;
                }

                switch (eventType)
                {
                    case MouseEventType.MouseDown:
                        if (MouseDown != null)
                        {
                            MouseDown(this, e);
                        }
                        break;
                    case MouseEventType.MouseUp:
                        if (Click != null)
                        {
                            Click(this, new EventArgs());
                        }
                        if (MouseUp != null)
                        {
                            MouseUp(this, e);
                        }
                        break;
                    case MouseEventType.DoubleClick:
                        if (DoubleClick != null)
                        {
                            DoubleClick(this, new EventArgs());
                        }
                        break;
                    case MouseEventType.MouseWheel:
                        if (MouseWheel != null)
                        {
                            MouseWheel(this, e);
                        }
                        break;
                    case MouseEventType.MouseMove:
                        if (MouseMove != null)
                        {
                            MouseMove(this, e);
                        }
                        break;
                    case MouseEventType.None:
                        break;
                    default:
                        break;
                }                 
                
            }

            return User32.CallNextHookEx(HandleToHook, nCode, wParam, lParam);

        }

        private static MouseButtons GetButton(Int32 wParam)
        {

            switch (wParam)
            {

                case (int)WindowMessages.WM_LBUTTONDOWN:
                case (int)WindowMessages.WM_LBUTTONUP:
                case (int)WindowMessages.WM_LBUTTONDBLCLK:
                    return MouseButtons.Left;
                case (int)WindowMessages.WM_RBUTTONDOWN:
                case (int)WindowMessages.WM_RBUTTONUP:
                case (int)WindowMessages.WM_RBUTTONDBLCLK:
                    return MouseButtons.Right;
                case (int)WindowMessages.WM_MBUTTONDOWN:
                case (int)WindowMessages.WM_MBUTTONUP:
                case (int)WindowMessages.WM_MBUTTONDBLCLK:
                    return MouseButtons.Middle;
                default:
                    return MouseButtons.None;

            }

        }

        private static MouseEventType GetEventType(Int32 wParam)
        {

            switch (wParam)
            {

                case (int)WindowMessages.WM_LBUTTONDOWN:
                case (int)WindowMessages.WM_RBUTTONDOWN:
                case (int)WindowMessages.WM_MBUTTONDOWN:
                    return MouseEventType.MouseDown;
                case (int)WindowMessages.WM_LBUTTONUP:
                case (int)WindowMessages.WM_RBUTTONUP:
                case (int)WindowMessages.WM_MBUTTONUP:
                    return MouseEventType.MouseUp;
                case (int)WindowMessages.WM_LBUTTONDBLCLK:
                case (int)WindowMessages.WM_RBUTTONDBLCLK:
                case (int)WindowMessages.WM_MBUTTONDBLCLK:
                    return MouseEventType.DoubleClick;
                case (int)WindowMessages.WM_MOUSEWHEEL:
                    return MouseEventType.MouseWheel;
                case (int)WindowMessages.WM_MOUSEMOVE:
                    return MouseEventType.MouseMove;
                default:
                    return MouseEventType.None;

            }
        }

        #endregion
    }

}
