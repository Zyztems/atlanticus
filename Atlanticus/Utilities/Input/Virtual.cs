﻿namespace Atlanticus.Utilities.Input
{
    public class Virtual
    {
        protected static int MakeLParam(int loWord, int hiWord)
        {
            return ((hiWord << 16) | (loWord & 0xffff));
        }
    }
}
