﻿using System;
using System.Threading;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;

namespace Atlanticus.Utilities.Input
{
    class VirtualBackgroundKeyboard: Virtual
    {
        private readonly IntPtr _hWnd;

        public VirtualBackgroundKeyboard(IntPtr hWnd)
        {
            _hWnd = hWnd;
        }

        private void KeyDown(VirtualKey virtualKey)
        {
            User32.PostMessage(_hWnd, (int)WindowMessages.WM_KEYDOWN, (IntPtr)virtualKey, (IntPtr)MakeLParam(1, User32.MapVirtualKey((int)virtualKey, 0)));
        }

        private void KeyUp(VirtualKey virtualKey)
        {
            User32.PostMessage(_hWnd, (int)WindowMessages.WM_KEYUP, (IntPtr)virtualKey, (IntPtr)MakeLParam(1, User32.MapVirtualKey((int)virtualKey, 0)));
        }

        private void SysKeyDown(VirtualKey virtualKey, int hexValue)
        {
            User32.PostMessage(_hWnd, (int)WindowMessages.WM_SYSKEYDOWN, (IntPtr)virtualKey, (IntPtr)hexValue);
        }

        private void SysKeyUp(VirtualKey virtualKey, int hexValue)
        {
            User32.PostMessage(_hWnd, (int)WindowMessages.WM_SYSKEYUP, (IntPtr)virtualKey, (IntPtr)hexValue);
        }

        public void KeyPress(VirtualKey virtualKey, bool shift, bool ctrl, bool alt)
        {
            if (shift)
                SysKeyDown(VirtualKey.VK_SHIFT, 0x0);
            
            if (ctrl)
                SysKeyDown(VirtualKey.VK_CONTROL, 0x0);

            if (alt)
                SysKeyDown(VirtualKey.VK_MENU, 0x0);

            KeyDown(virtualKey);
            Thread.Sleep(1);
            KeyUp(virtualKey);

            if (alt)
                SysKeyUp(VirtualKey.VK_MENU, 0x0);

            if (ctrl)
                SysKeyUp(VirtualKey.VK_CONTROL, 0x0);

            if (shift)
                SysKeyUp(VirtualKey.VK_SHIFT, 0x0);
        }
    }
}
