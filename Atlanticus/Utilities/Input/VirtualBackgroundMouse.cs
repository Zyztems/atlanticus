﻿using System;
using System.Drawing;
using System.Threading;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;

namespace Atlanticus.Utilities.Input
{
    public class VirtualBackgroundMouse : Virtual
    {
        private readonly IntPtr _hWnd;

        public VirtualBackgroundMouse(IntPtr hWnd)
        {
            _hWnd = hWnd;
        }

        public void Wheel(int delta)
        {
            User32.PostMessage(_hWnd, (int) WindowMessages.WM_VSCROLL, (IntPtr) (delta < 0 ? ScrollBar.SB_LINEDOWN : ScrollBar.SB_LINEUP), IntPtr.Zero);
        }

        public void LeftClick(Point point)
        {
            LeftClick(point.X, point.Y);
        }

        public void LeftClick(int x, int y)
        {
            LeftDown(x, y);
            LeftUp(x, y);
        }

        public void LeftDown(int x, int y)
        {
            User32.PostMessage(_hWnd, (int) WindowMessages.WM_LBUTTONDOWN, IntPtr.Zero, (IntPtr)MakeLParam(x, y));
        }

        public void LeftUp(int x, int y)
        {
            User32.PostMessage(_hWnd, (int) WindowMessages.WM_LBUTTONUP, IntPtr.Zero, (IntPtr)MakeLParam(x, y));
        }

        public void RightClick(IntPtr hWnd, int x, int y)
        {
            RightDown(x, y);
            RightUp(x, y);
        }

        public void RightDown(int x, int y)
        {
            User32.PostMessage(_hWnd, (int) WindowMessages.WM_RBUTTONDOWN, IntPtr.Zero, (IntPtr)MakeLParam(x, y));
        }

        public void RightUp(int x, int y)
        {
            User32.PostMessage(_hWnd, (int) WindowMessages.WM_RBUTTONUP, IntPtr.Zero, (IntPtr)MakeLParam(x, y));
        }

        public void LeftDrag(Point a, Point b)
        {
            LeftDown(a.X, a.Y);

            for (var i = a.Y; i <= b.Y; i++)
            {
                for (var j = a.X; j <= b.X; j++)
                {
                    Thread.Sleep(1);
                    Move(j, i);
                }
            }
            
            LeftUp(b.X, b.Y);
        }

        public void Move(int x, int y)
        {
            User32.PostMessage(_hWnd, (int) WindowMessages.WM_MOUSEMOVE, IntPtr.Zero, (IntPtr)MakeLParam(x, y));
        }
    }
}
