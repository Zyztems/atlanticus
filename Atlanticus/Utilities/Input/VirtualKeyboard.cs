﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;
using Atlanticus.Utilities.WindowsAPI.Structure;

namespace Atlanticus.Utilities.Input
{
    public static class VirtualKeyboard
    {
        private static WindowsAPI.Structure.Input KeyboardWrapper(ushort scanCode, bool keyUp)
        {

            var wrapper = new WindowsAPI.Structure.Input
                              {
                                  Type = SendInputType.Keyboard,
                                  MKH =
                                      {
                                          Keyboard = new KeyboardInputData
                                                         {
                                                             Key = 0,
                                                             Scan = scanCode,
                                                             Flags = (uint)KeyboardEvent.ScanCode,
                                                             Time = 0,
                                                             ExtraInfo = IntPtr.Zero
                                                         }
                                      }
                              };

            if (keyUp)
                wrapper.MKH.Keyboard.Flags |= (uint)KeyboardEvent.KeyUp;

            return wrapper;
        }

        public static void KeyPress(ushort scanCode, bool shift, bool ctrl, bool alt)
        {
            var inputs = new List<WindowsAPI.Structure.Input>
                             {
                                 KeyboardWrapper(scanCode, false),
                                 KeyboardWrapper(scanCode, true)
                             };

            if (shift)
            {
                inputs.Insert(0, KeyboardWrapper((ushort)VirtualKeyScanCode.DIK_LSHIFT, false));
                inputs.Add(KeyboardWrapper((ushort)VirtualKeyScanCode.DIK_LSHIFT, true));
            }

            if (ctrl)
            {
                inputs.Insert(0, KeyboardWrapper((ushort)VirtualKeyScanCode.DIK_LCONTROL, false));
                inputs.Add(KeyboardWrapper((ushort)VirtualKeyScanCode.DIK_LCONTROL, true));
            }

            if (alt)
            {
                inputs.Insert(0, KeyboardWrapper((ushort)VirtualKeyScanCode.DIK_LMENU, false));
                inputs.Add(KeyboardWrapper((ushort)VirtualKeyScanCode.DIK_LMENU, true));
            }

            User32.SendInput((uint) inputs.Count, inputs.ToArray(), Marshal.SizeOf(typeof (WindowsAPI.Structure.Input)));
        }

        public static void KeyPress(VirtualKey virtualKey, bool shift, bool ctrl, bool alt)
        {
            var inputs = new List<WindowsAPI.Structure.Input>
                             {
                                 KeyboardWrapper((ushort) User32.MapVirtualKey((int)virtualKey, 0), false),
                                 KeyboardWrapper((ushort) User32.MapVirtualKey((int)virtualKey, 0), true)
                             };

            if (shift)
            {
                inputs.Insert(0, KeyboardWrapper((ushort)User32.MapVirtualKey((int)VirtualKey.VK_LSHIFT, 0), false));
                inputs.Add(KeyboardWrapper((ushort)User32.MapVirtualKey((int)VirtualKey.VK_LSHIFT, 0), true));
            }

            if (ctrl)
            {
                inputs.Insert(0, KeyboardWrapper((ushort)User32.MapVirtualKey((int)VirtualKey.VK_LCONTROL, 0), false));
                inputs.Add(KeyboardWrapper((ushort)User32.MapVirtualKey((int)VirtualKey.VK_LCONTROL, 0), true));
            }

            if (alt)
            {
                inputs.Insert(0, KeyboardWrapper((ushort)User32.MapVirtualKey((int)VirtualKey.VK_LMENU, 0), false));
                inputs.Add(KeyboardWrapper((ushort)User32.MapVirtualKey((int)VirtualKey.VK_LMENU, 0), true));
            }

            User32.SendInput((uint)inputs.Count, inputs.ToArray(), Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }

        public static void KeyPress(ushort scanCode)
        {
            var inputs = new List<WindowsAPI.Structure.Input>
                             {
                                 KeyboardWrapper(scanCode, false),
                                 KeyboardWrapper(scanCode, true)
                             };

            User32.SendInput((uint)inputs.Count, inputs.ToArray(), Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }

        public static void KeyPress(VirtualKey virtualKey)
        {
            var inputs = new List<WindowsAPI.Structure.Input>
                             {
                                 KeyboardWrapper((ushort) User32.MapVirtualKey((int)virtualKey, 0), false),
                                 KeyboardWrapper((ushort) User32.MapVirtualKey((int)virtualKey, 0), true)
                             };

            User32.SendInput((uint)inputs.Count, inputs.ToArray(), Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }
    }
}