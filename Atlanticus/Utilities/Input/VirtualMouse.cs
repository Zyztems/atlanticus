﻿using System.Runtime.InteropServices;
using System.Threading;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;
using Atlanticus.Utilities.WindowsAPI.Structure;
using Point = System.Drawing.Point;

namespace Atlanticus.Utilities.Input
{
    public static class VirtualMouse
    {
        public static void Wheel(int delta)
        {
            var inputs = new[]
                             {
                                 new WindowsAPI.Structure.Input
                                     {
                                         Type = SendInputType.Mouse,
                                         MKH =
                                             {
                                                 Mouse = new MouseInputData
                                                             {
                                                                 MouseData = (uint) delta,
                                                                 Flags = MouseEvent.Wheel,
                                                             }
                                             }
                                     }
                             };

            User32.SendInput(1, inputs, Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }

        public static void LeftClick(Point point)
        {
            LeftClick(point.X, point.Y);
         }

        public static void LeftClick(int x, int y)
        {
            LeftDown(x, y);
            LeftUp(x, y);
        }

        public static void LeftDown(int x, int y)
        {
            Move(x, y);
            var inputs = new[]
                             {
                                 new WindowsAPI.Structure.Input
                                     {
                                         Type = SendInputType.Mouse,
                                         MKH =
                                             {
                                                 Mouse = new MouseInputData
                                                             {
                                                                 Flags =
                                                                     MouseEvent.LeftDown
                                                             }
                                             }
                                     }
                             };

            User32.SendInput(1, inputs, Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }

        public static void LeftUp(int x, int y)
        {
            Move(x, y);
            var inputs = new[]
                             {
                                 new WindowsAPI.Structure.Input
                                     {
                                         Type = SendInputType.Mouse,
                                         MKH =
                                             {
                                                 Mouse = new MouseInputData
                                                             {
                                                                 Flags =
                                                                     MouseEvent.LeftUp
                                                             }
                                             }
                                     }
                             };

            User32.SendInput(1, inputs, Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }

        public static void RightClick(int x, int y)
        {
            RightDown(x, y);
            RightUp(x, y);
        }

        public static void RightDown(int x, int y)
        {
            Move(x, y);
            var inputs = new[]
                             {
                                 new WindowsAPI.Structure.Input
                                     {
                                         Type = SendInputType.Mouse,
                                         MKH =
                                             {
                                                 Mouse = new MouseInputData
                                                             {
                                                                 Flags =
                                                                     MouseEvent.RightDown 
                                                             }
                                             }
                                     }
                             };

            User32.SendInput(1, inputs, Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }

        public static void RightUp(int x, int y)
        {
            Move(x, y);
            var inputs = new[]
                             {
                                 new WindowsAPI.Structure.Input
                                     {
                                         Type = SendInputType.Mouse,
                                         MKH =
                                             {
                                                 Mouse = new MouseInputData
                                                             {
                                                                 Flags =
                                                                     MouseEvent.RightDown 
                                                             }
                                             }
                                     }
                             };

            User32.SendInput(1, inputs, Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }

        public static void LeftDrag(Point a, Point b)
        {
            LeftDown(a.X, a.Y);

            for (var i = a.Y; i <= b.Y; i++)
            {
                for (var j = a.X; j <= b.X; j++)
                {
                    Thread.Sleep(1);
                    Move(j, i);
                }
            }
            
            LeftUp(b.X, b.Y);
        }

        public static void Move(int x, int y)
        {
            var inputs = new[]
                             {
                                 new WindowsAPI.Structure.Input
                                     {
                                         Type = SendInputType.Mouse,
                                         MKH =
                                             {
                                                 Mouse = new MouseInputData
                                                             {
                                                                 X = CalculateAbsoluteCoordinateX(x),
                                                                 Y = CalculateAbsoluteCoordinateY(y),
                                                                 Flags =
                                                                     MouseEvent.Absolute |
                                                                     MouseEvent.Move
                                                             }
                                             }
                                     }
                             };

            User32.SendInput(1, inputs, Marshal.SizeOf(typeof(WindowsAPI.Structure.Input)));
        }

        private static int CalculateAbsoluteCoordinateX(int x)
        {
            return (x * 65536) / User32.GetSystemMetrics(SystemMetric.SM_CXSCREEN);
        }

        private static int CalculateAbsoluteCoordinateY(int y)
        {
            return (y * 65536) / User32.GetSystemMetrics(SystemMetric.SM_CYSCREEN);
        }
    }
}