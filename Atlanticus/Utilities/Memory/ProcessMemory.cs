﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Structure;

namespace Atlanticus.Utilities.Memory
{
    public class ProcessMemory
    {
        private IntPtr _mhProcess;
        public readonly Process ReadProcess;

        /*
        public ProcessMemory(int processId)
        {
            var processesByName = Process.GetProcessById(processId);
            if (processesByName.Handle != IntPtr.Zero)
            {
                ReadProcess = processesByName;
                OpenProcess();
                Handled = true;
            }
            else
                Handled = false;
        }
        */

        public ProcessMemory(Process process)
        {
            ReadProcess = process;
            OpenProcess();
        }

        public void CloseHandle()
        {
            try
            {
                if (Kernel32.CloseHandle(_mhProcess) == 0)
                    throw new Exception("CloseHandle failed");
            }
            catch (Exception exception)
            {
                throw new Exception(exception.StackTrace);
            }
        }

        #region Read Adress
        public byte ReadByte(int iMemoryAddress)
        {
            //byte[] bBuffer = new byte[1]; 
            int ptrBytesRead;
            // int iReturn = .ReadProcessMemory((IntPtr)iMemoryAddress, 1, out ptrBytesRead);

            return ReadProcessMemory((IntPtr)iMemoryAddress, 1, out ptrBytesRead)[0];
        }
        public ushort ReadShort(int iMemoryAddress)
        {
            //byte[] bBuffer = new byte[2];
            int ptrBytesRead;
            //int iReturn = MApi.ReadProcessMemory(hReadProcess, (IntPtr)iMemoryAddress, bBuffer, 2, out ptrBytesRead);

            //if (iReturn == 0)
            //    return 0;
            //else
            return BitConverter.ToUInt16(ReadProcessMemory((IntPtr)iMemoryAddress, 2, out ptrBytesRead), 0);
        }
        public uint ReadInt(int iMemoryAddress)
        {
            //var bBuffer = new byte[4]; 
            int ptrBytesRead;
            //int iReturn = ReadProcessMemory(_mHProcess, (uint) iMemoryAddress, 4, out ptrBytesRead);

            //if (iReturn == 0)
            //    return 0;
            //else
            //    return BitConverter.ToUInt32(bBuffer, 0);

            return BitConverter.ToUInt16(ReadProcessMemory((IntPtr)iMemoryAddress, 4, out ptrBytesRead), 0);
        }
        #endregion

        #region Read from Pointer
        public byte ReadByte(int iMemoryAddress, int[] iOffsets)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesRead;
            return ReadProcessMemory((IntPtr)iFinalAddress, 1, out ptrBytesRead)[0];
        }

        public ushort ReadShort(int iMemoryAddress, int[] iOffsets)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesRead;
            return BitConverter.ToUInt16(ReadProcessMemory((IntPtr)iFinalAddress, 2, out ptrBytesRead), 0);
        }

        public uint ReadInt(int iMemoryAddress, int[] iOffsets)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesRead;
            return BitConverter.ToUInt32(ReadProcessMemory((IntPtr)iFinalAddress, 4, out ptrBytesRead), 0);
        }

        public long ReadLong(int iMemoryAddress, int[] iOffsets)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesRead;
            return BitConverter.ToInt64(ReadProcessMemory((IntPtr)iFinalAddress, 8, out ptrBytesRead), 0);
        }

        public float ReadFloat(int iMemoryAddress, int[] iOffsets)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesRead;
            return BitConverter.ToSingle(ReadProcessMemory((IntPtr)iFinalAddress, 4, out ptrBytesRead), 0);
        }

        public double ReadDouble(int iMemoryAddress, int[] iOffsets)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesRead;
            return BitConverter.ToDouble(ReadProcessMemory((IntPtr)iFinalAddress, 8, out ptrBytesRead), 0);
        }

        public string ReadText(int iMemoryAddress, int[] iOffsets, uint iStringLength, int iMode = 0)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesRead;
            var bBuffer = ReadProcessMemory((IntPtr)iFinalAddress, iStringLength, out ptrBytesRead);

            switch (iMode)
            {
                case 0:
                    return Encoding.Unicode.GetString(bBuffer);
                case 1:
                    return BitConverter.ToString(bBuffer).Replace("-", "");
                default:
                    return "";
            }
        }
        #endregion

        #region Write to Pointer
        public bool Write(int iMemoryAddress, int[] iOffsets, byte bByteToWrite)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesWritten;
            var bBuffer = new[] { bByteToWrite };
            WriteProcessMemory((IntPtr)iFinalAddress, bBuffer, out ptrBytesWritten);
            return (ptrBytesWritten == 1);
        }

        public bool Write(int iMemoryAddress, int[] iOffsets, short iShortToWrite)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesWritten;
            var bBuffer = BitConverter.GetBytes(iShortToWrite);
            WriteProcessMemory((IntPtr)iFinalAddress, bBuffer, out ptrBytesWritten);
            return (ptrBytesWritten == 2);
        }

        public bool Write(int iMemoryAddress, int[] iOffsets, int iIntToWrite)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesWritten;
            var bBuffer = BitConverter.GetBytes(iIntToWrite);
            WriteProcessMemory((IntPtr)iFinalAddress, bBuffer, out ptrBytesWritten);
            return (ptrBytesWritten == 4);
        }

        public bool Write(int iMemoryAddress, int[] iOffsets, long iLongToWrite)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesWritten;
            var bBuffer = BitConverter.GetBytes(iLongToWrite);
            WriteProcessMemory((IntPtr)iFinalAddress, bBuffer, out ptrBytesWritten);
            return (ptrBytesWritten == 8);
        }

        public bool Write(int iMemoryAddress, int[] iOffsets, float iFloatToWrite)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesWritten;
            var bBuffer = BitConverter.GetBytes(iFloatToWrite);
            WriteProcessMemory((IntPtr)iFinalAddress, bBuffer, out ptrBytesWritten);
            return (ptrBytesWritten == 4);
        }

        public bool Write(int iMemoryAddress, int[] iOffsets, double iDoubleToWrite)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesWritten;
            var bBuffer = BitConverter.GetBytes(iDoubleToWrite);
            WriteProcessMemory((IntPtr)iFinalAddress, bBuffer, out ptrBytesWritten);
            return (ptrBytesWritten == 8);
        }

        public bool Write(int iMemoryAddress, int[] iOffsets, string sStringToWrite, int iMode = 0)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesWritten; byte[] bBuffer = { 0 };

            switch (iMode)
            {
                case 0:
                    bBuffer = CreateAobText(sStringToWrite);
                    break;
                case 1:
                    bBuffer = ReverseBytes(CreateAobString(sStringToWrite));
                    break;
            }

            WriteProcessMemory((IntPtr)iFinalAddress, bBuffer, out ptrBytesWritten);
            return (ptrBytesWritten == sStringToWrite.Length);
        }

        public bool Write(int iMemoryAddress, int[] iOffsets, byte[] bBytesToWrite)
        {
            var iFinalAddress = CalculatePointer(iMemoryAddress, iOffsets);
            int ptrBytesWritten;
            WriteProcessMemory((IntPtr)iFinalAddress, bBytesToWrite, out ptrBytesWritten);
            return (ptrBytesWritten == bBytesToWrite.Length);
        }
        #endregion

        #region Private Methods
        private void OpenProcess()
        {
            _mhProcess =
                Kernel32.OpenProcess(
                (uint)(ProcessAccessType.ProcessVmWrite |
                       ProcessAccessType.ProcessVmRead |
                       ProcessAccessType.ProcessVmOperation), 1, (uint)ReadProcess.Id);
        }

        private byte[] ReadProcessMemory(IntPtr memoryAddress, uint bytesToRead, out int bytesRead)
        {
            IntPtr ptr;
            var buffer = new byte[bytesToRead];
            Kernel32.ReadProcessMemory(_mhProcess, memoryAddress, buffer, bytesToRead, out ptr);
            bytesRead = ptr.ToInt32();
            return buffer;
        }

        private void WriteProcessMemory(IntPtr memoryAddress, byte[] bytesToWrite, out int bytesWritten)
        {
            IntPtr ptr;
            Kernel32.WriteProcessMemory(_mhProcess, memoryAddress, bytesToWrite, (uint)bytesToWrite.Length, out ptr);
            bytesWritten = ptr.ToInt32();
        }

        private static byte[] ReverseBytes(IList<byte> bOriginalBytes)
        {
            var iBytes = bOriginalBytes.Count; var bNewBytes = new byte[iBytes];

            for (var i = 0; i < iBytes; i++)
            {
                bNewBytes[iBytes - i - 1] = bOriginalBytes[i];
            }
            return bNewBytes;
        }

        private static byte[] CreateAobText(string sBytes)
        {
            return Encoding.ASCII.GetBytes(sBytes);
        }

        private static byte[] CreateAobString(string sBytes)
        {
            return BitConverter.GetBytes(Dec(sBytes));
        }

        private static int Dec(string sHex)
        {
            return Int32.Parse(sHex, NumberStyles.HexNumber);
        }

        private int CalculatePointer(int iMemoryAddress, IList<int> iOffsets)
        {
            var iPointerCount = iOffsets.Count - 1;

            var iTemporaryAddress = iPointerCount == 0 ? iMemoryAddress : 0;

            for (var i = 0; i <= iPointerCount; i++)
            {
                int ptrBytesRead;
                if (i == iPointerCount)
                    return BitConverter.ToInt32(ReadProcessMemory((IntPtr)iTemporaryAddress, 4, out ptrBytesRead), 0) + iOffsets[i];
                if (i == 0)
                    iTemporaryAddress = BitConverter.ToInt32(ReadProcessMemory((IntPtr)iMemoryAddress, 4, out ptrBytesRead), 0) + iOffsets[0];
                else
                    iTemporaryAddress = BitConverter.ToInt32(ReadProcessMemory((IntPtr)iTemporaryAddress, 4, out ptrBytesRead), 0) + iOffsets[i];
            }
            return 0;
        }
        #endregion
    }
}
