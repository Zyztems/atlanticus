﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Atlanticus.Utilities.WindowControl;
using SlimDX.Direct3D9;

//using Utilities.WindowsAPI;

namespace Atlanticus.Utilities.Screen
{
    public class Direct3DCapture
    {
        private WindowStates _windowStates;

        private readonly Dictionary<IntPtr, Device> _direct3DDeviceCache;
        private readonly Direct3D _direct3D9;
        private Device _device;
        
        public Direct3DCapture()
        {
            _direct3D9 = new Direct3D();
            _direct3DDeviceCache = new Dictionary<IntPtr, Device>();
            _device = new Device(_direct3D9, 0, DeviceType.Hardware, IntPtr.Zero,
                                            CreateFlags.SoftwareVertexProcessing,
                                            new PresentParameters
                                            {
                                                Windowed = true,
                                                SwapEffect = SwapEffect.Discard,
                                            });
        }

        public Bitmap CaptureWindow(IntPtr hWnd)
        {
            _windowStates = new WindowStates(hWnd);
            return CaptureRegionDirect3D(hWnd, WindowPosition.GetAbsoluteClientRectangle(hWnd));
        }

        //private Bitmap CaptureRegionDirect3D(Rectangle region)
        //{
        //    using (var surface = Surface.CreateOffscreenPlain(_device, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height, Format.A8R8G8B8, Pool.Scratch))
        //    {
        //        _device.GetFrontBufferData(0, surface);

        //        return new Bitmap(Surface.ToStream(surface, ImageFileFormat.Bmp, new Rectangle(region.Left, region.Top, region.Width, region.Height)));
        //    }
        //}

        private static bool ValidateRegion(Rectangle region)
        {
            return (region.Left >= 0 && region.Top >= 0 && region.Width >= 0 && region.Height >= 0);
        }

        private Bitmap CaptureRegionDirect3D(IntPtr handle, Rectangle region)
        {
            if (!ValidateRegion(region))
            {
                _windowStates.Restore();
            }

            Bitmap bitmap;
            var adapterInfo = _direct3D9.Adapters.DefaultAdapter;

            #region Get Direct3D Device
            if (_direct3DDeviceCache.ContainsKey(handle))
            {
                _device = _direct3DDeviceCache[handle];
            }
            else
            {
                var clientRect = WindowPosition.GetAbsoluteClientRectangle(handle);
                var parameters = new PresentParameters
                                     {
                                        BackBufferFormat = adapterInfo.CurrentDisplayMode.Format,
                                        BackBufferHeight = clientRect.Height,
                                        BackBufferWidth = clientRect.Width,
                                        Multisample = MultisampleType.None,
                                        SwapEffect = SwapEffect.Discard,
                                        DeviceWindowHandle = handle,
                                        PresentationInterval = PresentInterval.Default,
                                        FullScreenRefreshRateInHertz = 0
                                     };

                _device = new Device(_direct3D9, adapterInfo.Adapter, DeviceType.Hardware, handle, CreateFlags.SoftwareVertexProcessing, parameters);
                _direct3DDeviceCache.Add(handle, _device);
            }
            #endregion
 
            using (var surface = Surface.CreateOffscreenPlain(_device, adapterInfo.CurrentDisplayMode.Width, adapterInfo.CurrentDisplayMode.Height, Format.A8R8G8B8, Pool.SystemMemory))
            {
                _device.GetFrontBufferData(0, surface);
                bitmap = new Bitmap(Surface.ToStream(surface, ImageFileFormat.Bmp, new Rectangle(region.Left < 0 ? 0 : region.Left, 
                                                                                                 region.Top < 0 ? 0 : region.Top, 
                                                                                                 region.Width < 0 ? 0 : region.Width,
                                                                                                 region.Height < 0 ? 0 : region.Height)));
            }
 
            return bitmap;
        }
    }
}