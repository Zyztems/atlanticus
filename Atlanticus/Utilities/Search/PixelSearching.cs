﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace Atlanticus.Utilities.Search
{
    public static class PixelSearching
    {
        public static Point PixelSearch(Bitmap bmp, Color pixelColor)
        {
            return PixelSearch(bmp, pixelColor, 0);
        }

        public static Point PixelSearch(Bitmap bmp, Color pixelColor, Point point)
        {
            return PixelSearch(bmp, pixelColor, point.X, point.Y);
        }

        public static unsafe Point PixelSearch(Bitmap bmp, Color pixelColor, int shadeVariation)
        {
            var bitmapdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            var numArray = new int[] { pixelColor.B, pixelColor.G, pixelColor.R };
            for (var i = 0; i < bitmapdata.Height; i++)
            {
                var numPtr = ((byte*)bitmapdata.Scan0) + (i * bitmapdata.Stride);

                for (var j = 0; j < bitmapdata.Width; j++)
                {
                    if (
                        ((!((numPtr[j * 3] >= (numArray[0] - shadeVariation)) &
                            (numPtr[j * 3] <= (numArray[0] + shadeVariation)))) ||
                         (!((numPtr[(j * 3) + 1] >= (numArray[1] - shadeVariation)) &
                            (numPtr[(j * 3) + 1] <= (numArray[1] + shadeVariation))))) ||
                        (!((numPtr[(j * 3) + 2] >= (numArray[2] - shadeVariation)) &
                           (numPtr[(j * 3) + 2] <= (numArray[2] + shadeVariation))))) continue;

                    return new Point(j, i);
                }
            }
            return new Point();
        }

        public static unsafe bool HasPixel(Bitmap bmp, Color pixelColor, int shadeVariation)
        {
            if (bmp == null) throw new ArgumentNullException("bmp");

            var bitmapdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            var numArray = new int[] { pixelColor.B, pixelColor.G, pixelColor.R };
            
            for (var i = 0; i < bitmapdata.Height; i++)
            {
                var numPtr = ((byte*)bitmapdata.Scan0) + (i * bitmapdata.Stride);
                
                for (var j = 0; j < bitmapdata.Width; j++)
                {
                    if (
                        ((!((numPtr[j * 3] >= (numArray[0] - shadeVariation)) &
                            (numPtr[j * 3] <= (numArray[0] + shadeVariation)))) ||
                         (!((numPtr[(j * 3) + 1] >= (numArray[1] - shadeVariation)) &
                            (numPtr[(j * 3) + 1] <= (numArray[1] + shadeVariation))))) ||
                        (!((numPtr[(j * 3) + 2] >= (numArray[2] - shadeVariation)) &
                           (numPtr[(j * 3) + 2] <= (numArray[2] + shadeVariation))))) continue;

                    bmp.UnlockBits(bitmapdata);
                    //bmp.Dispose();
                    //GC.Collect(0, GCCollectionMode.Forced);
                    //GC.WaitForFullGCComplete();
                    return true;
                }
            }

            bmp.UnlockBits(bitmapdata);
            //bmp.Dispose();
            //GC.Collect(0, GCCollectionMode.Forced);
            //GC.WaitForFullGCComplete();
            return false;
        }

        public static unsafe List<Point> ListPixelSearch(Bitmap bmp, Color pixelColor, int shadeVariation)
        {
            var bitmapdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            var numArray = new int[] { pixelColor.B, pixelColor.G, pixelColor.R };
            var pointList = new List<Point>();
            for (var i = 0; i < bitmapdata.Height; i++)
            {
                var numPtr = ((byte*)bitmapdata.Scan0) + (i * bitmapdata.Stride);

                for (var j = 0; j < bitmapdata.Width; j++)
                {
                    if (
                        ((!((numPtr[j * 3] >= (numArray[0] - shadeVariation)) &
                            (numPtr[j * 3] <= (numArray[0] + shadeVariation)))) ||
                         (!((numPtr[(j * 3) + 1] >= (numArray[1] - shadeVariation)) &
                            (numPtr[(j * 3) + 1] <= (numArray[1] + shadeVariation))))) ||
                        (!((numPtr[(j * 3) + 2] >= (numArray[2] - shadeVariation)) &
                           (numPtr[(j * 3) + 2] <= (numArray[2] + shadeVariation))))) continue;

                    pointList.Add(new Point(j, i));
                }
            }
            return pointList;
        }

        public static unsafe Point PixelSearch(Bitmap bmp, Color pixelColor, int notIncludedX, int notIncludedY)
        {
            var bitmapdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            var numArray = new int[] { pixelColor.B, pixelColor.G, pixelColor.R };
            for (var i = 0; i < bitmapdata.Height; i++)
            {
                var numPtr = (((byte*)bitmapdata.Scan0) + (i * bitmapdata.Stride));

                for (var j = 0; j < bitmapdata.Width; j++)
                {
                    if (((!((numPtr[j * 3] >= numArray[0]) & (numPtr[j * 3] <= numArray[0]))) ||
                         (!((numPtr[(j * 3) + 1] >= numArray[1]) & (numPtr[(j * 3) + 1] <= numArray[1])))) ||
                        (!((numPtr[(j * 3) + 2] >= numArray[2]) & (numPtr[(j * 3) + 2] <= numArray[2])))) continue;

                    var point = new Point(j, i);

                    if (point == new Point(notIncludedX, notIncludedY)) continue;

                    bmp.UnlockBits(bitmapdata);
                    return point;
                }
            }
            return new Point();
        }

        public static Color GetPixelColor(Point point)
        {
            return GetPixelColor(point.X, point.Y);
        }

        static public Color GetPixelColor(int x, int y)
        {
            IntPtr hdc = WindowsAPI.User32.GetDC(IntPtr.Zero);
            uint pixel = WindowsAPI.User32.GetPixel(hdc, x, y);
            WindowsAPI.User32.ReleaseDC(IntPtr.Zero, hdc);
            Color color = Color.FromArgb((int)(pixel & 0x000000FF),
                                         (int)(pixel & 0x0000FF00) >> 8,
                                         (int)(pixel & 0x00FF0000) >> 16);
            return color;
        }
    }
}

