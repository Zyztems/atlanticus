﻿using System;
using System.Collections;

namespace Atlanticus.Utilities.WindowControl
{
    public class WindowCollection : ReadOnlyCollectionBase
    {
        public void Add(IntPtr hWnd)
        {
            var item = new WindowStates(hWnd);
            InnerList.Add(item);
        }

        public WindowStates this[int index]
        {
            get
            {
                return (WindowStates) InnerList[index];
            }
        }
    }
}

