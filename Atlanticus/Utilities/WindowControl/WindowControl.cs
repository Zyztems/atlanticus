﻿using System;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;
using Atlanticus.Utilities.WindowsAPI.Structure;

namespace Atlanticus.Utilities.WindowControl
{
    internal class WindowControl
    {
        public static bool IsFullScreen(IntPtr hWnd)
        {
            WindowRectangle windowRectangle;
            WindowRectangle windowRectangle2;
            User32.GetWindowRect(hWnd, out windowRectangle);
            User32.GetClientRect(hWnd, out windowRectangle2);
            return (((windowRectangle2.Bottom == windowRectangle.Bottom) &&
                     (windowRectangle2.Right == windowRectangle.Right)) ||
                    (((User32.GetSystemMetrics(SystemMetric.SM_CMONITORS) > 1) &&
                      (windowRectangle2.Bottom == windowRectangle.Bottom)) &&
                     (windowRectangle2.Right == (windowRectangle.Right - windowRectangle.Left))));
        }

        public static WindowStates GetChildWindow(string title, string className)
        {
            if ((title != "") || (className != ""))
            {
                var windows = new Window();
                windows.GetWindows();
                foreach (WindowStates item in windows.Items)
                {
                    if ((className == "") && (item.Text == title))
                    {
                        return item;
                    }
                    if ((item.Text == title) && (item.ClassName == className))
                    {
                        return item;
                    }
                    if ((title == "") && (item.ClassName == className))
                    {
                        return item;
                    }
                }
            }
            return null;
        }
    }
}