﻿using System;
using System.Drawing;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Structure;
using Point = System.Drawing.Point;

namespace Atlanticus.Utilities.WindowControl
{
    public static class WindowPosition
    {
        public static Rectangle GetAbsoluteClientRectangle(IntPtr hWnd)
        {
            var windowRect = GetWindowRect(hWnd);
            var clientRect = GetClientRect(hWnd);

            // This gives us the width of the left, right and bottom chrome - we can then determine the top height
            var chromeWidth = (windowRect.Width - clientRect.Width) / 2;
            
            return new Rectangle(new Point(windowRect.X + chromeWidth, windowRect.Y + (windowRect.Height - clientRect.Height - chromeWidth)), clientRect.Size);
        }

        /// <summary>
        /// Get a windows client rectangle in a .NET structure
        /// </summary>
        /// <param name="hwnd">The window handle to look up</param>
        /// <returns>The rectangle</returns>
        private static Rectangle GetClientRect(IntPtr hwnd)
        {
            WindowRectangle rect;
            User32.GetClientRect(hwnd, out rect);
            return rect;
        }

        /// <summary>
        /// Get a windows rectangle in a .NET structure
        /// </summary>
        /// <param name="hwnd">The window handle to look up</param>
        /// <returns>The rectangle</returns>
        private static Rectangle GetWindowRect(IntPtr hwnd)
        {
            WindowRectangle rect;
            User32.GetWindowRect(hwnd, out rect);
            return rect;
        }
    }
}
