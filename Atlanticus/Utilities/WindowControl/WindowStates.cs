﻿using System;
using System.Drawing;
using System.Text;
using Atlanticus.Utilities.WindowsAPI;
using Atlanticus.Utilities.WindowsAPI.Enumeration;
using Atlanticus.Utilities.WindowsAPI.Structure;
using Point = System.Drawing.Point;

namespace Atlanticus.Utilities.WindowControl
{
    public class WindowStates
    {
        private readonly IntPtr _hWnd = IntPtr.Zero;

        public WindowStates(IntPtr hWnd)
        {
            _hWnd = hWnd;
        }

        public override int GetHashCode()
        {
            return (int) _hWnd;
        }

        public void Restore()
        {
            if (Iconic)
            {
                User32.SendMessage(_hWnd, 0x112, (IntPtr)0xf120, IntPtr.Zero);
            }
            User32.BringWindowToTop(_hWnd);
            User32.SetForegroundWindow(_hWnd);
        }

        public string ClassName
        {
            get
            {
                var lpClassName = new StringBuilder(260, 260);
                User32.GetClassName(_hWnd, lpClassName, lpClassName.Capacity);
                return lpClassName.ToString();
            }
        }

        public ExtendedWindowStyle ExtendedWindowStyle
        {
            get
            {
                return (ExtendedWindowStyle)User32.GetWindowLong(_hWnd, -20);
            }
        }

        public IntPtr Handle
        {
            get
            {
                return _hWnd;
            }
        }

        public bool Iconic
        {
            get
            {
                return (User32.IsIconic(_hWnd) != 0);
            }
            set
            {
                User32.SendMessage(_hWnd, 0x112, (IntPtr)0xf020, IntPtr.Zero);
            }
        }

        public Point Location
        {
            get
            {
                Rectangle rect = Rect;
                return new Point(rect.Left, rect.Top);
            }
        }

        public bool Maximised
        {
            get
            {
                return (User32.IsZoomed(_hWnd) != 0);
            }
            set
            {
                User32.SendMessage(_hWnd, 0x112, (IntPtr)0xf030, IntPtr.Zero);
            }
        }

        public Rectangle Rect
        {
            get
            {
                WindowRectangle lpRect;
                User32.GetWindowRect(_hWnd, out lpRect);
                return lpRect; //new Rectangle(lpRect.Left, lpRect.Top, lpRect.Right - lpRect.Left, lpRect.Bottom - lpRect.Top);
            }
        }

        public Size Size
        {
            get
            {
                var rect = Rect;
                return new Size(rect.Right - rect.Left, rect.Bottom - rect.Top);
            }
        }

        public string Text
        {
            get
            {
                var lpString = new StringBuilder(260, 260);
                User32.GetWindowText(_hWnd, lpString, lpString.Capacity);
                return lpString.ToString();
            }
        }

        public bool Visible
        {
            get
            {
                return (User32.IsWindowVisible(_hWnd) != 0);
            }
        }

        public WindowStyle WindowStyle
        {
            get
            {
                return (WindowStyle)User32.GetWindowLong(_hWnd, -16);
            }
        }
    }
}

