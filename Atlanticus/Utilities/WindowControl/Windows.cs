﻿using System;
using Atlanticus.Utilities.WindowsAPI;

namespace Atlanticus.Utilities.WindowControl
{
    public class Window
    {
        private WindowCollection _items;

        public void GetWindows()
        {
            _items = new WindowCollection();
            User32.EnumWindows(WindowEnum, 0);
        }

        public void GetWindows(IntPtr hWndParent)
        {
            _items = new WindowCollection();
            User32.EnumChildWindows(hWndParent, WindowEnum, 0);
        }

        protected virtual bool OnWindowEnum(IntPtr hWnd)
        {
            _items.Add(hWnd);
            return true;
        }

        private int WindowEnum(IntPtr hWnd, int lParam)
        {
            if (OnWindowEnum(hWnd))
            {
                return 1;
            }
            return 0;
        }

        public WindowCollection Items
        {
            get
            {
                return _items;
            }
        }
    }
}

