using System;

namespace Atlanticus.Utilities.WindowsAPI.Enumeration
{
    [Flags]
    public enum KeyboardEvent : uint
    {
        ExtendedKey = 0x1,
        KeyUp = 0x2,
        Unicode = 0x4,
        ScanCode = 0x8
    }
}