using System;

namespace Atlanticus.Utilities.WindowsAPI.Enumeration
{
    [Flags]
    public enum MouseData : uint
    {
        XButton1 = 0x0001,
        XButton2 = 0x0002
    }
}