﻿namespace Atlanticus.Utilities.WindowsAPI.Enumeration
{
    public enum ScrollBar
    {
        SB_LINEUP = 0, // Scrolls one line up
        SB_LINELEFT = 0,// Scrolls one cell left
        SB_LINEDOWN = 1, // Scrolls one line down
        SB_LINERIGHT = 1,// Scrolls one cell right
        SB_PAGEUP = 2, // Scrolls one page up
        SB_PAGELEFT = 2,// Scrolls one page left
        SB_PAGEDOWN = 3, // Scrolls one page down
        SB_PAGERIGTH = 3, // Scrolls one page right
        SB_PAGETOP = 6, // Scrolls to the upper left
        SB_LEFT = 6, // Scrolls to the left
        SB_PAGEBOTTOM = 7, // Scrolls to the upper right
        SB_RIGHT = 7, // Scrolls to the right
        SB_ENDSCROLL = 8 // Ends scroll
    }
}
