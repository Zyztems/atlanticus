namespace Atlanticus.Utilities.WindowsAPI.Enumeration
{
    public enum SendInputType : int
    {
        Mouse = 0,
        Keyboard,
        Hardware
    }
}