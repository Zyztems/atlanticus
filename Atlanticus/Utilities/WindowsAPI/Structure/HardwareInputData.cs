using System.Runtime.InteropServices;

namespace Atlanticus.Utilities.WindowsAPI.Structure
{
    [StructLayout(LayoutKind.Sequential)]
    public struct HardwareInputData
    {
        public int Msg;
        public short ParamL;
        public short ParamH;
    }
}