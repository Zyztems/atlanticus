using System.Runtime.InteropServices;
using Atlanticus.Utilities.WindowsAPI.Enumeration;

namespace Atlanticus.Utilities.WindowsAPI.Structure
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Input
    {
        public SendInputType Type;
        public MouseKeyboardHardwareUnion MKH;
    }
}