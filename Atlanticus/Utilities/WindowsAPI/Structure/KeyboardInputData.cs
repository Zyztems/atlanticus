using System;
using System.Runtime.InteropServices;

namespace Atlanticus.Utilities.WindowsAPI.Structure
{
    [StructLayout(LayoutKind.Sequential)]
    public struct KeyboardInputData
    {
        public ushort Key;
        public ushort Scan;
        public uint Flags;
        public uint Time;
        public IntPtr ExtraInfo;
    }
}