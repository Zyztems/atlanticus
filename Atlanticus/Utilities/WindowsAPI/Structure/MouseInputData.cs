using System;
using System.Runtime.InteropServices;
using Atlanticus.Utilities.WindowsAPI.Enumeration;

namespace Atlanticus.Utilities.WindowsAPI.Structure
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MouseInputData
    {
        public int X;
        public int Y;
        public uint MouseData;
        public MouseEvent Flags;
        public uint Time;
        public IntPtr ExtraInfo;
    }
}