using System.Runtime.InteropServices;

namespace Atlanticus.Utilities.WindowsAPI.Structure
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MouseLlHookStruct
    {
        public Point pt;
        public int mouseData;
        public int flags;
        private int time;
        private int dwExtraInfo;
    }
}