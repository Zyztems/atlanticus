using System;
using System.Runtime.InteropServices;

namespace Atlanticus.Utilities.WindowsAPI.Structure
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Point
    {
        public Int32 x;
        public Int32 y;
    }
}