using System;

namespace Atlanticus.Utilities.WindowsAPI.Structure
{
    [Flags]
    public enum ProcessAccessType
    {
        ProcessTerminate = 0x0001,
        ProcessCreateThread = 0x0002,
        ProcessSetSessionid = 0x0004,
        ProcessVmOperation = 0x0008,
        ProcessVmRead = 0x0010,
        ProcessVmWrite = 0x0020,
        ProcessDupHandle = 0x0040,
        ProcessCreateProcess = 0x0080,
        ProcessSetQuota = 0x0100,
        ProcessSetInformation = 0x0200,
        ProcessQueryInformation = 0x0400
    }
}