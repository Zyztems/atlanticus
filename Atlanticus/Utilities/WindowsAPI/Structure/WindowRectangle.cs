﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Atlanticus.Utilities.WindowsAPI.Structure
{
    [StructLayout(LayoutKind.Sequential)]
    public struct WindowRectangle
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
        public int Width
        {
            get
            {
                return Math.Abs(Right - Left);
            }
        }
        public int Height
        {
            get
            {
                return Math.Abs(Top - Bottom);
            }
        }
        public static implicit operator Rectangle(WindowRectangle windowRectangle)
        {
            return Rectangle.FromLTRB(windowRectangle.Left, windowRectangle.Top, windowRectangle.Right, windowRectangle.Bottom);
        }

        public static implicit operator WindowRectangle(Rectangle rect)
        {
            return new WindowRectangle(rect.Left, rect.Top, rect.Right, rect.Bottom);
        }

        public WindowRectangle(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }
    }

}

